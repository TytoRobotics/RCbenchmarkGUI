"use strict";

/* This object allows you to store data in memory. The data is store in the 
dynamic memory for fast access. You can store variables and objects. 
Initialisation example

// Example defaut values
var defaultValues = {
    'systemVMax': 5,
    'vMin': 0,
    'vMax': 2
};

var storageVariables = new StorageVariables('uniqueInternalID',
    defaultValues, afterMemoryLoading);

function afterMemoryLoading() {
    storageVariables.set('systemVMax', 6);
    // The following value will always be 6, even after program restart and the 
    // previous line is commented.
    console.log(storageVariables.get('systemVMax'));
}
*/

// Constructor
function StorageVariables(categoryId, defaultVal, callback) {
    // The objects created from the protype will inherit this:
    this.values = defaultVal;
    this.categoryId = categoryId;
    this._isInitialized = false;
    // If the ID is unique 
    if (StorageVariables.definedCategories.indexOf(categoryId) === -1) {
        StorageVariables.definedCategories.push();
    } else {
        throw 'Category is not unique ';
    }

    //load values from memory
    // checks if ID key already exists in storage, if not use this default value
    StorageVariables._loadValuesInMemory(this.values, this.categoryId, callback, this);
}

StorageVariables._loadValuesInMemory =
    function _loadValuesInMemory(values, categoryId, callback, that) {
        function _setDefaultValues(inMemoryValues) {
            var attrname;
            for (attrname in inMemoryValues[that.categoryId]) {
                // Check that attrname is a property of inMemoryValue only.
                if (inMemoryValues[that.categoryId].hasOwnProperty(attrname)) {
                    // Properties in memory are copied over the default values.
                    if (values.hasOwnProperty(attrname)) {
                        values[attrname] = inMemoryValues[that.categoryId][attrname];
                    }
                }
            }
            var event = new Event('build');
            document.dispatchEvent(event);
            that._isInitialized = true;
            if (callback) {
                callback();
            }
        }
        chrome.storage.local.get(categoryId, _setDefaultValues);
    };

StorageVariables.definedCategories = [];

// This function will merge the default values and the values in memory.
// The result is saved in this.values. The values in memory are given priority 
// over the default values.
StorageVariables.prototype.setDefaultValues =
    function setDefaultValues(inMemoryValues, values) {
        var attrname;
        for (attrname in inMemoryValues) {
            // Check that attrname is a property of inMemoryValue only.
            if (inMemoryValues.hasOwnProperty(attrname)) {
                // Properties in memory are copied over the default values.
                if (values.hasOwnProperty(attrname)) {
                    console.log(inMemoryValues[attrname]);
                    values[attrname] = inMemoryValues[attrname];
                }
            }
        }
    };

StorageVariables.prototype.set = function (property, val, callback) {
    if (this._isInitialized) {
        if (this.values.hasOwnProperty(property)) {
            this.values[property] = val;
        }
        var storage = {};
        storage[this.categoryId] = this.values;
        chrome.storage.local.set(storage, callback);
    } else {
        throw "Object did not have time to initialize. Use the callback in the constructor";
    }
};

StorageVariables.prototype.get = function (property, callback) {
    function obtainValue(storageVariableObj, callback) {
        if (storageVariableObj.values.hasOwnProperty(property)) {
            callback(storageVariableObj.values[property]);
        } 
        else throw "Trying to access unknown property " + property
    }
    if (this._isInitialized) {
        obtainValue(this, callback);
    } 
    else {
        document.addEventListener('build', function() {obtainValue(this, callback)}, false);
        throw "Object did not have time to initialize. Use the callback in the constructor";S
    }
};





