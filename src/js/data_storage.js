'use strict';

var CONFIGURATOR = {
    'releaseDate': 1573248551913, // type "new Date().getTime()" in the console. Will return the new number.
    'firmwareVersionAccepted15xx': '1.19',
    'firmwareVersionAccepted1780': '2.10',
    'backupFileMinVersionAccepted': '0.70', // chrome.runtime.getManifest().version is stored as string, so does this one
    'connectionValid': false,
};

var CONFIG = {
    developper_mode: false,
    scripting_mode: false,
    database_mode: true,
    safetyCutoffDisable: false,
    workingDirectory: undefined,
    firmwareVersion: 'unknown',
    gravityConstant: 9.80665,
    tempProbesQty: 0,
    pSensorAvailable: 0,
    s1780detected:{
      LCA: false,
      LCB: false,
      PSA: false,
      PSB: false
    }
};

// See memorymanagemendet.js
// Do no add something to main.js anymore.
// Access with CONFIG.xxx.value
// Set with changeMemory(CONFIG.xxx, newValue);
CONFIG.electricalRPMActive = new storedVariable(true, 'electricalRPMactive');
CONFIG.opticalRPMActive = new storedVariable(true, 'opticalRPMactive');
CONFIG.numberOfMotorPoles = new storedVariable(12, 'pole_number');
CONFIG.numberOfOpticalTape = new storedVariable(1, 'optical_number');
CONFIG.mainRPMsensor = new storedVariable('electrical', 'mainRPMsensor');
CONFIG.temperatureProbes = new storedVariable({}, 'temperatureProbes');
CONFIG.ESCMin = new storedVariable(1000, 'ESCMin');
CONFIG.ESCMax = new storedVariable(2000, 'ESCMax');
CONFIG.ESCCutoff = new storedVariable(1000, 'ESCCutoff');   //for the 1520 1580
CONFIG.ESCCutoffA = new storedVariable(1000, 'ESCCutoffA'); //for the 1780 A side
CONFIG.ESCCutoffB = new storedVariable(1000, 'ESCCutoffB'); //for the 1780 B side
CONFIG.servo1Min = new storedVariable(1000, 'servo1Min');
CONFIG.servo1Max = new storedVariable(2000, 'servo1Max');
CONFIG.servo2Min = new storedVariable(1000, 'servo2Min');
CONFIG.servo2Max = new storedVariable(2000, 'servo2Max');
CONFIG.servo3Min = new storedVariable(1000, 'servo3Min');
CONFIG.servo3Max = new storedVariable(2000, 'servo3Max');
CONFIG.ESCAMin = new storedVariable(1000, 'ESCAMin');
CONFIG.ESCAMax = new storedVariable(2000, 'ESCAMax');
CONFIG.ESCBMin = new storedVariable(1000, 'ESCBMin');
CONFIG.ESCBMax = new storedVariable(2000, 'ESCBMax');
CONFIG.ServoAMin = new storedVariable(1000, 'ServoAMin');
CONFIG.ServoAMax = new storedVariable(2000, 'ServoAMax');
CONFIG.ServoBMin = new storedVariable(1000, 'ServoBMin');
CONFIG.ServoBMax = new storedVariable(2000, 'ServoBMax');
CONFIG.reverseThrust = new storedVariable(false, 'reverseThrust');
CONFIG.reverseTorque = new storedVariable(false, 'reverseTorque');
CONFIG.reverseThrustB = new storedVariable(false, 'reverseThrustB');
CONFIG.reverseTorqueB = new storedVariable(false, 'reverseTorqueB');
CONFIG.csvNotes = new storedVariable('', 'csvNotes');
CONFIG.pressureTare = new storedVariable(0, 'pressureTare');
CONFIG.currentTare15xx = new storedVariable(0, 'currentTare15xx');
CONFIG.currentScale15xx = new storedVariable(1.0, 'currentScale15xx');
CONFIG.currentTareA = new storedVariable(0, 'currentTareA');
CONFIG.currentTareB = new storedVariable(0, 'currentTareB');
CONFIG.pressureCal = new storedVariable(1, 'pressureCal');
CONFIG.atmPressure = new storedVariable(101325, 'atmPressure');
CONFIG.plotsRateHz = new storedVariable(25, 'plotsRefreshRateHz');
CONFIG.updateAvailable = new storedVariable(false,'updateAvailable');
CONFIG.updateNotify = new storedVariable(true,'updateNotify');
CONFIG.safetyNotify = new storedVariable('','safetyNotify');
CONFIG.latest_version = new storedVariable('','latest_version');
CONFIG.reverseCutoffSwitch = new storedVariable(false,'reverseCutoffSwitch');
CONFIG.numberOfOpticalTapeB = new storedVariable(1, 'optical_numberB');
CONFIG.controlBoard = new storedVariable({protocol:'pwm_50'}, 'controlBoard');
CONFIG.databaseAdvanced = new storedVariable(false, 'databaseAdvanced');

var HACKS; //used to save the debug hack codes
var DB_SCRIPT_PARAMS; //used to save latest database script parameters

//https://oscarliang.com/esc-firmware-protocols/
var CONTROL_PROTOCOLS = {
  pwm_50: {
    min_val: 700,
    max_val: 2300,
    default_min: 1000,
    default_max: 2000,
    default_cutoff: 1000,
    //configurable_cutoff_val: true,
    microseconds: true,
    msp_min: 700,
    msp_max: 2300
  },
  pwm_100: {
    min_val: 700,
    max_val: 2300,
    default_min: 1000,
    default_max: 2000,
    default_cutoff: 1000,
    //configurable_cutoff_val: true,
    microseconds: true,
    msp_min: 700,
    msp_max: 2300
  },
  pwm_200: {
    min_val: 700,
    max_val: 2300,
    default_min: 1000,
    default_max: 2000,
    default_cutoff: 1000,
    //configurable_cutoff_val: true,
    microseconds: true,
    msp_min: 700,
    msp_max: 2300
  },
  pwm_300: {
    min_val: 700,
    max_val: 2300,
    default_min: 1000,
    default_max: 2000,
    default_cutoff: 1000,
    //configurable_cutoff_val: true,
    microseconds: true,
    msp_min: 700,
    msp_max: 2300
  },
  pwm_400: {
    min_val: 700,
    max_val: 2300,
    default_min: 1000,
    default_max: 2000,
    default_cutoff: 1000,
    //configurable_cutoff_val: true,
    microseconds: true,
    msp_min: 700,
    msp_max: 2300
  },
  pwm_500: {
    min_val: 700,
    max_val: 2000, //limited to 2000 because of refresh rate
    default_min: 1000,
    default_max: 2000,
    default_cutoff: 1000,
    //configurable_cutoff_val: true,
    microseconds: true,
    msp_min: 700,
    msp_max: 2000
  },
  dshot150: {
    min_val: 0,
    max_val: 2047,
    default_min: 0,
    default_max: 2047,
    default_cutoff: 0,
    msp_min: 0,
    msp_max: 2047
    //configurable_cutoff_val: false,
  },
  dshot300: {
    min_val: 0,
    max_val: 2047,
    default_min: 0,
    default_max: 2047,
    default_cutoff: 0,
    msp_min: 0,
    msp_max: 2047
    //configurable_cutoff_val: false,
  },
  dshot600: {
    min_val: 0,
    max_val: 2047,
    default_min: 0,
    default_max: 2047,
    default_cutoff: 0,
    msp_min: 0,
    msp_max: 2047
    //configurable_cutoff_val: false,
  },
  dshot1200: {
    min_val: 0,
    max_val: 2047,
    default_min: 0,
    default_max: 2047,
    default_cutoff: 0,
    msp_min: 0,
    msp_max: 2047
    //configurable_cutoff_val: false,
  },
  multishot: {
    min_val: 5,
    max_val: 25,
    default_min: 5,
    default_max: 25,
    default_cutoff: 5,
    msp_min: 60,
    msp_max: 300,
    microseconds: true,
    //configurable_cutoff_val: false,
  },
  oneshot42: {
    min_val: 42,
    max_val: 84,
    default_min: 42,
    default_max: 84,
    default_cutoff: 42,
    microseconds: true,
    msp_min: 1000,
    msp_max: 2000,
    //configurable_cutoff_val: true,
  },
  oneshot125: { // https://oscarliang.com/oneshot125-esc-quadcopter-fpv/
    min_val: 125,
    max_val: 250,
    default_min: 125,
    default_max: 250,
    default_cutoff: 125,
    microseconds: true,
    msp_min: 1000,
    msp_max: 2000,
    //configurable_cutoff_val: true,
  },
};

//1580 1520
var SYSTEM_LIMITS_15xx = {
    voltageMin: 0,		    //[V]
    voltageMax: 35,			//[V]
    currentMin: -40,		//[A]
    currentMax: 40,			//[A]
    currentBurstMin: -50,		//[A]
    currentBurstMax: 50,	//[A]
    powerMin: 0,			//[W]
    powerMax: 1400,			//[W]
    rpmMin: 0,
    rpmMax: 0, //calculated upon init
    thrustMin: -5,			//[kg]
    thrustMax: 5,			//[kg]
    torqueMin: -2,			//[Nm]
    torqueMax: 2,			//[Nm]
    vibrationMin: 0,			//[g]
    vibrationMax: 8,			//[g]
    temperatureMin: -10,        //[C]
    temperatureMax: 120,         //[C]
    PWMTimeMin: 0,         // us Change in rcbApi.js too!
    PWMTimeMax: 2300         // us Change in rcbApi.js too!
};
//var ORIGINAL_SYSTEM_LIMITS_15xx = $.extend(true, {}, SYSTEM_LIMITS_15xx);

//1780
var SYSTEM_LIMITS_1780 = {
    voltageMin: 0,		    //[V]
    voltageMaxA: 0,			//[V]
    voltageMaxB: 0,			//[V]
    currentMinA: 0,		//[A]
    currentMaxA: 0,			//[A]
    currentBurstMinA: 0,		//[A]
    currentBurstMaxA: 0,	//[A]
    currentMinB: 0,		//[A]
    currentMaxB: 0,			//[A]
    currentBurstMinB: 0,		//[A]
    currentBurstMaxB: 0,	//[A]
    powerMinA: 0,			//[W]
    powerMaxA: 0,			//[W]
    powerMinB: 0,			//[W]
    powerMaxB: 0,			//[W]
    rpmMin: 0,
    rpmMax: 0, //calculated upon init
    rpmMaxB: 0, //calculated upon init
    thrustMinA: 0,			//[kg]
    thrustMaxA: 0,			//[kg]
    torqueMinA: 0,			//[Nm]
    torqueMaxA: 0,			//[Nm]
    thrustMinB: 0,			//[kg]
    thrustMaxB: 0,			//[kg]
    torqueMinB: 0,			//[Nm]
    torqueMaxB: 0,			//[Nm]
    PWMTimeMin: 700,         // us Change in rcbApi.js too!
    PWMTimeMax: 2300,         // us Change in rcbApi.js too!
    boardSpecific: {			//DIFFERENT SETTINGS FOR OTHER CIRCUIT VERSIONS
      // NOTE THE VERSIONS MATCH WITH THE 1780 FIRMWARE, see TestPlatformFirmware.ino around line 208
      powerSensorV1: {
        currentMin: -100,			//[A]
        currentMax: 100,			//[A]
        currentBurstMin: -140,		//[A]
        currentBurstMax: 140,	//[A]
        powerMax: 8000,			//[W]
        voltageMax: 60         //[V]
      },
      powerSensorV2: {
        currentMin: -1,			//[A]
        currentMax: 150,			//[A]
        currentBurstMin: -1,		//[A]
        currentBurstMax: 180,	//[A]
        powerMax: 10000,			//[W]
        voltageMax: 60         //[V]
      },
      powerSensorV3: {
        currentMin: -150,			//[A]
        currentMax: 150,			//[A]
        currentBurstMin: -180,		//[A]
        currentBurstMax: 180,	//[A]
        powerMax: 10000,			//[W]
        voltageMax: 60         //[V]
      },
      powerSensorV4: {
        currentMin: -500,			//[A]
        currentMax: 500,			//[A]
        currentBurstMin: -500,		//[A]
        currentBurstMax: 500,	//[A]
        powerMax: 50000,			//[W]
        voltageMax: 100         //[V]
      },
      loadSensorV1: { // 
        thrustMin: -25,			//[kg]
        thrustMax: 25,			//[kg]
        torqueMin: -12,			//[Nm]
        torqueMax: 12,			//[Nm]
      },
      loadSensorV2: {
        thrustMin: -25,			//[kg]
        thrustMax: 25,			//[kg]
        torqueMin: -12,			//[Nm]
        torqueMax: 12,			//[Nm]
      },
      loadSensorV3: {
        thrustMin: -40,			//[kg]
        thrustMax: 40,			//[kg]
        torqueMin: -18,			//[Nm]
        torqueMax: 18,			//[Nm]
      },
      loadSensorV4: {
        thrustMin: -75,			//[kg]
        thrustMax: 75,			//[kg]
        torqueMin: -48,			//[Nm]
        torqueMax: 48,			//[Nm]
      }
    }
};
//var ORIGINAL_SYSTEM_LIMITS_1780 = $.extend(true, {}, SYSTEM_LIMITS_1780);

//DEFAULT USER LIMITS
var USER_LIMITS = {
    voltageMin: 0, //[V]
    voltageMax: 15, //[V]
    voltageMinA: 0, //[V]
    voltageMaxA: 15, //[V]
    currentMin: -0.1, //[A]
    currentMax: 3, //[A]
    currentBurstMin: -0.1, //[A]
    currentBurstMax: 6, //[A]
    powerMin: 0, //[W]
    powerMax: 0, //[W]
    powerMinA: 0, //[W]
    powerMaxA: 100, //[W]
    rpmMin: 0,
    rpmMax: 10000,
    thrustMin: -1, //[kg]
    thrustMax: 1, //[kg]
    torqueMin: -0.5, //[Nm]
    torqueMax: 0.5, //[Nm]
    voltageMinB: 0, //[V]
    voltageMaxB: 15, //[V]
    currentMinB: -0.1, //[A]
    currentMaxB: 3, //[A]
    currentBurstMinB: -0.1, //[A]
    currentBurstMaxB: 6, //[A]
    powerMinB: 0, //[W]
    powerMaxB: 100, //[W]
    rpmMinB: 0,
    rpmMaxB: 10000,
    thrustMinB: -1, //[kg]
    thrustMaxB: 1, //[kg]
    torqueMinB: -0.5, //[Nm]
    torqueMaxB: 0.5, //[Nm]
    vibrationMin: 0, //[g]
    vibrationMax: 2, //[g]
    temperatureMin: 10, //[C] default only for new probes
    temperatureMax: 60, //[C] default only for new probes
    temperaturesMin: {}, //[C] but matched with a specific probe id
    temperaturesMax: {}, //[C] but matched with a specific probe id
    refreshRate: 0
};

var SENSOR_DATA = {
    accelerometer:  [0,0,0],
    vibration: 0,
    debug:          [0,0,0,0],
    pinState:          [0,0,0,0], //ESC, Servo1, Servo2, Servo3
    loadCellThrust: 0,
    loadCellLeft: 0,
    loadCellRight: 0,
    ESCvoltage: 0,
    ESCcurrent: 0,
    eRPM_HZ: 0,
    oRPM_HZ: 0,
    power:0,
    proDataFlag: 0,
    basicDataFlag: 0,
    updateRate: 0,
    lastOhm: NaN,
    rawPressureP: 0,
    rawPressureT: 0,
    sixAxisForcesNA: [0,0,0,0,0,0],
    sixAxisForcesRawA: [0,0,0,0,0,0],
    temperature: [
        {id: "", value: 0},{id: "", value: 0},{id: "", value: 0},{id: "", value: 0},{id: "", value: 0},{id: "", value: 0}
    ],
    s1780Bvoltage: 0,
    s1780Bcurrent: 0,
    sixAxisForcesNB: [0,0,0,0,0,0],
    sixAxisForcesRawB: [0,0,0,0,0,0],
    s1780loadCellOverload: 0,
    s1780limitSwitch: 0,
    s1780BopticalRPM: 0
};

var FIRMWARE_FLAGS = {
    escVoltageFlag: 0,
    escCurrentFlag: 0,
    power:0,
    loadCellThrustFlag: 0,
    loadCellLeftFlag: 0,
    loadCellRightFlag:0,
    eRPMFlag:0,
    oRPMFlag:0,
    accelerometerFlag:0,
    temperatureProbes:0
}

var LOAD_CELLS_CALIBRATION = {
    thrustNominal:  5, //kg (value of the load cell)
    leftRightNominal:  2, //kg (value of the load cell)
    fullScaleVoltage:   5, //mV (full scale sensitivity)         
    hingeDistance: 0.07492,   // [m] distance between the two hinge axes
    calibrationWeight: 0.2,	//[kg]
    calibrationWeightDistance: 0.13,       //[m]
    calibrationFactorLeft:   1.0,  // should be close to unity
    calibrationFactorRight:  1.0,            
    calibrationFactorThrust: 1.0,
    calibrationFactorHingeLeft:  1.0,
    calibrationFactorHingeRight:  1.0             
};

var TARE_OFFSET = {
   loadCellThrustResetValue: 0,   // [mV]
   loadCellRightResetValue: 0,    // [mV]
   loadCellLeftResetValue: 0,     // [mV]
   sixAxisForcesNA: [0,0,0,0,0,0],  // FX FY FZ, TX, TY, TZ (N and Nm)
   sixAxisForcesNB: [0,0,0,0,0,0],  // FX FY FZ, TX, TY, TZ (N and Nm)
   tareTime: 2500 //time to stabilize the lpf in ms.
};

//Filter cutoff frequency in Hz. Ie, everything above the value will be filtered out as noise.
//Write 0 to disable the filter
var FILTER_CONFIG = {
    lpfLeftAlpha: 1,
    lpfRightAlpha: 1,
    lpfThrustAlpha: 1,
    lpfCurrentAlpha: 1,
    lpfCurrentBurstAlpha: 1/20, //Acts like a slow fuse, allowing short duration burst current. UPDATE in burstcurrent.html if changed.
    lpfVoltageAlpha: 1,
    lpfMotorRpmAlpha: 20,
    lpfMotorOpticalRpmAlpha: 20,
    lpfPower: 1,
    lpfPressure : 0.2,
    lpf1780ADCAlpha : 1 // Same for all so that reaction time for current/voltage/forces is the same
    //lpf1780CurrentOffsetAlpha: 5 // For the current auto-tare
};

var OUTPUT_DATA = {
    ESC_PWM:        1000,
    Servo_PWM:      [1500,1500,1500],
    ESCA: 1000,
    ServoA: 1500,
    ESCB: 1000,
    ServoB: 1500,
    active:         [0,0,0,0]
};

var PROCESSED_DATA = {
    ESCcurrent:     0,
    motor_RPM: 0
};

var LPFThrust = new LowPassFilter(FILTER_CONFIG.lpfThrustAlpha);
var LPFRight = new LowPassFilter(FILTER_CONFIG.lpfRightAlpha);
var LPFLeft = new LowPassFilter(FILTER_CONFIG.lpfLeftAlpha);
var LPFescCurrent = new LowPassFilter(FILTER_CONFIG.lpfCurrentAlpha);
var LPFescCurrentBurst = new LowPassFilter(FILTER_CONFIG.lpfCurrentBurstAlpha);
var LPFescVoltage = new LowPassFilter(FILTER_CONFIG.lpfVoltageAlpha);
var LPFmotorRPM = new LowPassFilter(FILTER_CONFIG.lpfMotorRpmAlpha);
var LPFmotorOpticalRPM = new LowPassFilter(FILTER_CONFIG.lpfMotorOpticalRpmAlpha);
var LPFpower = new LowPassFilter(FILTER_CONFIG.lpfPower);
var LPFpressure = new LowPassFilter(FILTER_CONFIG.lpfPressure);
var LPFpressureT = new LowPassFilter(FILTER_CONFIG.lpfPressure);

// 1780
var LPFescCurrentA = new LowPassFilter(FILTER_CONFIG.lpf1780ADCAlpha);
var LPFescCurrentOffsetA = new LowPassFilter(FILTER_CONFIG.lpf1780CurrentOffsetAlpha);
var LPFescCurrentBurstA = new LowPassFilter(FILTER_CONFIG.lpfCurrentBurstAlpha);
var LPFescVoltageA = new LowPassFilter(FILTER_CONFIG.lpf1780ADCAlpha);
var LPFescCurrentB = new LowPassFilter(FILTER_CONFIG.lpf1780ADCAlpha);
var LPFescCurrentOffsetB = new LowPassFilter(FILTER_CONFIG.lpf1780CurrentOffsetAlpha);
var LPFescCurrentBurstB = new LowPassFilter(FILTER_CONFIG.lpfCurrentBurstAlpha);
var LPFescVoltageB = new LowPassFilter(FILTER_CONFIG.lpf1780ADCAlpha);
var LPFforcesA = [];
var LPFmotorOpticalRPMB = new LowPassFilter(FILTER_CONFIG.lpfMotorOpticalRpmAlpha);
for (var i=0;i<6;i++){
  LPFforcesA[i] = new LowPassFilter(FILTER_CONFIG.lpf1780ADCAlpha);
}
var LPFforcesB = [];
for (var i=0;i<6;i++){
  LPFforcesB[i] = new LowPassFilter(FILTER_CONFIG.lpf1780ADCAlpha);
}


var UNITS = {
   fullText:{  //how each unit will be printed in the dropdown box
		N: "Newton",
        Nm: "Newton meter",
		dyn: "Dyne",
		dynm: "Dyne meter",
        kg: "Kilogram",
		kgf: "Kilogram-force",
		kgfm: "Kilogram-force meter",
        g: "Gram",
		gf: "Gram-force",
		gfm: "Gram-force meter",
		oz: "Ounce",
		ozf: "Ounce-force",
        ozfin: "Ounce-force inch",
        ozfft: "Ounce-force foot",
		lb: "Pound",
		lbf: "Pound-force",
		lbfin: "Pound-force inch",
		lbfft: "Pound-force foot",
        rpm: "Revolutions per minute",
		Hz: "Hertz",
		rads: "Radian per second",
		W: "Watts",
        C: "Celcius",
        F: "Fahrenheit",
        K: "Kelvin",
        mps: "Meters per second",
        fts: "Feet per second",
        kph: "Kilometers per hour",
        mph: "Miles per hour",
        Pa: "Pascal",
        inH2O: "Inches of water",
        mmH2O: "Milimeters of water"
   }, 
   text:{  //how each unit will be printed in text
        N: "N",
        Nm: "N·m",
        dyn: "dyn",
        dynm: "dyn·m",
        kg: "kg",
        kgf: "kgf",
        kgfm: "kgf·m",
        g: "g",
        gf: "gf",
        gfm: "gf·m",
        oz: "oz",
        ozf: "ozf",
        ozfin: "ozf·in",
        ozfft: "ozf·ft",
        lb: "lb",
        lbf: "lbf",
        lbfin: "lbf·in",
        lbfft: "lbf·ft",
        rpm: "RPM",
        Hz: "Hz",
        rads: "rad/s",
        W: "W",
        C: "ºC",
        F: "ºF",
        K: "K",
        mps: "m/s",
        fts: "ft/s",
        kph: "km/h",
        mph: "mph",
        Pa: "Pa",
        inH2O: "in. H2O",
        mmH2O: "mm H2O"
    }, 
   choices:{  //those are in the dropdown lists for each sensor
        thrust: ["kgf","gf","ozf","lbf","N"], //unit chosen by user, but when GUI is loaded these will be defaults if nothing saved in storage
        torque: ["Nm","ozfin","ozfft","lbfin","lbfft","kgfm","gfm"],
        motorSpeed: ["rpm", "Hz", "rads"],
        weight: ["kg","g","oz","lb","N"],
		power: ["W"],
        temperature: ["C","F","K"],
        speed: ["mps", "fts", "kph", "mph"],
        pressure: ["Pa", "inH2O", "mmH2O"]
    },
    working:{  //units used in the backend of the GUI.
        thrust: "kgf", //units that the GUI is working with internally.
        torque: "Nm",
        motorSpeed: "rpm",
        weight: "kg",
        power: "W",
        temperature: "C",
        speed: "mps",
        pressure: "Pa"
        
    },
    display:{  //units shown to the user. Those are loaded/saved in chrome storage
        thrust: "kgf", //unit chosen by user, but when GUI is loaded these will be defaults if nothing saved in storage
        torque: "Nm",
        motorSpeed: "rpm",
        weight: "kg",
        power: "W",
        temperature: "C",
        speed: "mps",
        pressure: "Pa"
    },
    conversions:{ 
        //conversion coefficients for converting from working units -> display units. 
        //Use a function for other than scaling (ie temperature)
        //If using a function, conversions from other directions must be supplied (cannot do 1/coeff for reciprocal)
        kg:{ //base unit -> GUI uses these internally
            g: 1000, //display units (1000g in 1 kg)
            lb: 2.20462262,
            oz: 35.2739619,
			N: 9.80665002864,
			dyn: 980665.002864,
        },
		kgf:{ //base unit -> GUI uses these internally
            gf: 1000, //display units (1000gf in 1 kgf)
            lbf: 2.20462262,
            ozf: 35.2739619,
			N: 9.80665002864,
			dyn: 980665.002864,
        },
        Nm:{
            ozfin: 141.611928936,
            ozfft: 11.800994356504876,
            lbfin: 8.85074576738,
            lbfft: 0.737562149277,
			kgfm: 0.10197162129779283,
			gfm: 101.97162129779283,
			dynm: 100000,
        },
		rpm:{
			Hz: 0.016666666667,
			rads: 0.104719755120,
		},
        C:{
            F: function(C){
                return (C*9/5) + 32;
            },
            K: function(C){
                return C + 273.15;
            }
        },
        F:{
            C: function(F){
                return (F - 32) * 5/9;
            },
            K: function(F){
                return (F - 32) * 5/9 + 273.15;
            }
        },
        K:{
            C: function(K){
                return K - 273.15;
            },
            F: function(K){
                return (K - 273.15) * 9/5 + 32;
            }
        },
        mps:{
            fts: function(mps){
                return mps * 3.28084;
            },
            kph: function(mps){
                return mps * 3.6;
            },
            mph: function(mps){
                return mps * 2.2369363636364;
            }
        },
        Pa:{
            inH2O: function(Pa){
                return Pa / 248.84;
            },
            mmH2O: function(Pa){
                return Pa / 9.80665;
            }
        },
    },
    //converts the sensor value to user's unit system
    convertToDisplay: function (sensor, value){  //sensor would be the sensor in question, like "thrust"
		if(value === ''){
			return '';
		}else{
			var display = UNITS.display[sensor];
			var working = UNITS.working[sensor];
			if (working != display) {
				if(typeof value !== 'undefined'){
					var coefficient = UNITS.conversions[working][display];
                    var converted;
                    if(typeof(coefficient) === 'function'){
                        converted = coefficient(value);
                    }else{
                        converted = value * coefficient;
                    }
					return converted;
				}
			} else {
				return value * 1;
			}
		}
        return 0;
     },
	 //converts the sensor value to units used in the backend of the GUI
    convertToWorking: function (sensor, value){  //sensor would be the sensor in question, like "thrust"
		if(value === ''){
			return 0;
		}else{
			var display =  UNITS.display[sensor];
			var working = UNITS.working[sensor];
			if (working != display) {
				if(typeof value !== 'undefined'){
					var coefficient = UNITS.conversions[working][display];
                    var converted = 0;
                    if(typeof(coefficient) === 'function'){
                        //Get the reciprocal function instead
                        coefficient = UNITS.conversions[display][working];
                        converted = coefficient(value);
                    }else{
                        converted = value / coefficient;
                    }
					return converted;
				}
			} else {
				return value * 1;
			}
		}
     },
     //returns the unit used by the user for a specific sensor
     getDisplayUnit: function (sensor){  //sensor would be the sensor in question, like "thrust"
          return UNITS.display[sensor];
     },
    //returns the text corresponding to an unit
    getUnitText: function (unit){  //unit is the unit id, like "kg"
          return UNITS.text[unit];
     },
    //returns the full text corresponding to an unit (to be displayed in the dropdown box)
    getUnitFullText: function (unit){  //unit is the unit id, like "kg"
          return UNITS.fullText[unit] + " [" + UNITS.getUnitText(unit) + "]";
     }
};
