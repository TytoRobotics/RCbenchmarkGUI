'use strict';

var SCRIPT_RUNNING = false;
var SCRIPT_ENVIRONMENT = '';
var AUTO_SCRIPT = {};

//Loads the scripting tab content
function loadScriptingTab(callback){
	if(CONFIG.scripting_mode){
		$('#tabAutomatic').load("./tabs/autocontrol/autocontrol.html", function () {
			AUTO_SCRIPT.script = "";
			AUTO_SCRIPT.name = "";
			translateAll();

			//Fill in list of scripts
			TABS.autocontrol.fillScriptList();

			//Select default
			$('.tab-autocontrol select.dropdownScripts').val('?Custom');

			TABS.autocontrol.newFile = false;

			function changeScript(){
				var selected_option = String($('.tab-autocontrol select.dropdownScripts').val());

				if(selected_option==="?Custom"){
					//Load the script editor
					TABS.autocontrol.loadScriptEditor();
				}else{
					function loadScriptViewer(){
						//Load the script viewer
						TABS.autocontrol.loadBottomFromFiles("./tabs/autocontrol/scriptViewer.html","./tabs/autocontrol/scriptViewer.css",function(){
							var editor = ace.edit("autocontrol-editor");
							editor.setTheme("ace/theme/chrome");
							editor.setShowPrintMargin(false);
							editor.container.style.opacity=0.7;
							editor.getSession().setUseWrapMode(true);
							editor.setReadOnly(true);
							editor.$blockScrolling = Infinity;
							editor.setValue(AUTO_SCRIPT.script,-1);
							editor.getSession().setMode("ace/mode/javascript");	
							$('a.clone-button').show();
						});
					}

					//Determine the type of script and load it from file
					var val = $('select.dropdownScripts').val();
					if(val[0]==='?'){
						//Stock script
						AUTO_SCRIPT.name = val.substring(1);
						$.get('../scripts/' + AUTO_SCRIPT.name + '.js', function(data) {
							AUTO_SCRIPT.script = data;
							loadScriptViewer();
						}, "text");
						$('a.edit-button').hide();
					}else{
						//User script
						AUTO_SCRIPT.name = val;
						$('a.edit-button').show();
						CONFIG.workingDirectory.getFile(AUTO_SCRIPT.name + '.js', {}, function(fileEntry) {
							// Get a File object representing the file,
							// then use FileReader to read its contents.
							fileEntry.file(function(file) {
							   var reader = new FileReader();
							   reader.onloadend = function(e) {
								 AUTO_SCRIPT.script = this.result;
								 loadScriptViewer();
							   };
							   reader.readAsText(file);
							}, TABS.autocontrol.fileReadError);
						}, TABS.autocontrol.fileReadError);
					}
				}
			}

			//Callback for dropdown change
			$('.tab-autocontrol select.dropdownScripts').change(function () {
				changeScript();
			});		

			changeScript();

			//Fill in the run script window
			$('.tab-autocontrol div.runScriptWindow').load("./tabs/autocontrol/runScript.html",function(){
				translateAll();

				//Apply the css
				$("head").append($("<link rel='stylesheet' href='./tabs/autocontrol/runScript.css' type='text/css' media='screen' />"));

				$('.tab-autocontrol #autocontrol-start-button').addClass('start-state');
				$('.tab-autocontrol #autocontrol-start-button').click(function(){
                    console.log("Running");
					runScript(AUTO_SCRIPT.script, AUTO_SCRIPT.name);
				});
			});
        
			//Run script button callback
			TABS.autocontrol.runView = false;
            $('.tab-autocontrol a.run-button').html(getMessage('runScriptButton'));
			$('.tab-autocontrol a.run-button').click(function(){
				//Toggle between views
				if(TABS.autocontrol.runView){
					clearAutoConsole();
					$('.tab-autocontrol select.dropdownScripts').removeClass('ui-block');
					$('.tab-autocontrol a.edit-button').removeClass('ui-block');
					$('.tab-autocontrol a.clone-button').removeClass('ui-block');
					$('.tab-autocontrol div.runScriptWindow').hide();
					$('.tab-autocontrol div.lowerScriptContent').show();
					TABS.autocontrol.runView = false;
					$('.tab-autocontrol a.run-button').html(getMessage('runScriptButton'));
				}else{
					$('.tab-autocontrol select.dropdownScripts').addClass('ui-block');
					$('.tab-autocontrol a.edit-button').addClass('ui-block');
					$('.tab-autocontrol a.clone-button').addClass('ui-block');
					$('.tab-autocontrol div.runScriptWindow').show();
					$('.tab-autocontrol div.lowerScriptContent').hide();
					TABS.autocontrol.runView = true;
					$('.tab-autocontrol a.run-button').html(getMessage('returnToEdit'));
				}
			});

			//Copy and edit button callback
			$('a.clone-button').click(function(){
				TABS.autocontrol.loadScriptEditor(AUTO_SCRIPT.script);
                googleAnalytics.sendEvent('ScriptClone', 'Click');
			});

			//Edit button callback
			$('a.edit-button').click(function(){
				TABS.autocontrol.loadScriptEditor(AUTO_SCRIPT.script,true);
                googleAnalytics.sendEvent('ScriptEdit', 'Click');
			});

			if (callback) callback();
		});
    }else{
    	$('#tabAutomatic').load("./tabs/autocontrol/scriptingDisabled.html", function () {
			translateAll();
			if (callback) callback();
		});
    }
}

//Called everytime a USB poll is received
function script_update(){
	scriptSendMessage('pollUpdate');	
}

//Called by the rest of the GUI to report errors towards the script
function scriptReportError(message){
	scriptSendMessage('GUIerror',{message: message});
}

function scriptReportWarning(message){
	scriptSendMessage('GUIwarning',{message: message});
}

function scriptReportMessage(message){
	scriptSendMessage('GUImessage',{message: message});
}

function scriptSendMessage(command_, message_){
	if(SCRIPT_RUNNING){
		//Update the sensors
		var iframe = $('#sandboxFrame');
		var message = {
		   command: command_,
		   content: message_
		};
		iframe[0].contentWindow.postMessage(message, '*');
	}
}

TABS.autocontrol = {};

TABS.autocontrol.testFilename = function(name){
	return /^[0-9a-zA-Z\^\&\'\@\{\}\[\]\,\$\=\!\-\#\(\)\.\%\+\~\_ ]+$/.test(name);
}

//Fill in the list of scripts
TABS.autocontrol.fillScriptList = function(){
	var getList = function(){
		TABS.autocontrol.comboAdd('Custom','?Custom','New custom script');
		TABS.autocontrol.comboAdd('Stock scripts','?Get number of poles','Get number of poles');
		TABS.autocontrol.comboAdd('Stock scripts','?Measure KV','Measure KV');
		TABS.autocontrol.comboAdd('Stock scripts','?Sweep - continuous','Sweep - continuous');
		TABS.autocontrol.comboAdd('Stock scripts','?Sweep - discrete','Sweep - discrete');
        TABS.autocontrol.comboAdd('Stock scripts','?Sweep - discrete V2','Sweep - discrete V2');
        TABS.autocontrol.comboAdd('Stock scripts','?Fixed sample rate','Fixed sample rate');
        TABS.autocontrol.comboAdd('Stock scripts','?Custom steps sequence','Custom steps sequence');
        TABS.autocontrol.comboAdd('Stock scripts','?90% settling time','90% settling time');
        TABS.autocontrol.comboAdd('Stock scripts','?Network communication','Network UDP');
        
		//Get the list of existing user scripts
		if(isDirectory()){	
			if(TABS.autocontrol.fillScriptList.lastList === undefined) TABS.autocontrol.fillScriptList.lastList = {};
			Object.keys(TABS.autocontrol.fillScriptList.lastList).forEach(function (key) {
			   TABS.autocontrol.fillScriptList.lastList[key].delete = true;
			});
			var dirReader = CONFIG.workingDirectory.createReader();
			var entries = [];
			var readEntries = function() {
				dirReader.readEntries (function(results) {
				  if (results.length) {
					results.forEach(function(entry){
						if(entry.isFile){
							var filename = entry.fullPath.split('\\').pop().split('/').pop(); //Remove the folder portion
							var ext = filename.split('.').pop();
							var file = filename.split('.').shift();
							if(ext === 'js' && TABS.autocontrol.testFilename(file)){ //Ensure file respects criteria
								//Mark as existing
								if(TABS.autocontrol.fillScriptList.lastList[file] === undefined) TABS.autocontrol.fillScriptList.lastList[file] = {};
								TABS.autocontrol.fillScriptList.lastList[file].delete = false;
								TABS.autocontrol.comboAdd('User scripts',file,file + '.js');
							}
						}
					});
					readEntries(); //Iterate until folder all scanned
				  }else{
					//remove deleted files from list
				  	Object.keys(TABS.autocontrol.fillScriptList.lastList).forEach(function (key) {
					   if(TABS.autocontrol.fillScriptList.lastList[key].delete){
					   	TABS.autocontrol.comboRemove('User scripts',key);
					   }
					});
				  }
				}, TABS.autocontrol.fileError);
			};
			readEntries(); // Start reading dirs.
		}
		TABS.autocontrol.handleFileName();
	};
	getList();
	setInterval(getList, 1000);
}

TABS.autocontrol.handleFileName = function(htmlLoaded){
	if(!TABS.autocontrol.handleFileName.loaded || htmlLoaded){
		//handle the filename box
		if(isDirectory()){	
			$('p#scriptFileSaveStatus').html(getMessage('pleaseSetFileName'));
			$('input#scriptFileName').removeClass('ui-block');
			$('input#scriptFileName').change(function(){
				var newFileName = $('input#scriptFileName').val();
				if (!TABS.autocontrol.testFilename(newFileName)) {
					$('p#scriptFileSaveStatus').html('<span style="color:orange">' + getMessage("filenameInvalid") + '</span>');	
				}else{
					if(AUTO_SCRIPT.customName===""){
						AUTO_SCRIPT.customName = newFileName;
						TABS.autocontrol.customScriptSave(getMessage('newFileSaved', [newFileName+".js"]));
					}else{
						if(!(AUTO_SCRIPT.customName === newFileName)){
							function rename(cwd, src, newName) {
							  cwd.getFile(src, {}, function(fileEntry) {
								fileEntry.moveTo(cwd, newName);
							  }, TABS.autocontrol.fileWriteError);
							}
							rename(CONFIG.workingDirectory, AUTO_SCRIPT.customName + '.js', newFileName + '.js');
							$('p#scriptFileSaveStatus').html(getMessage('fileRenamed', [AUTO_SCRIPT.customName+".js", newFileName+".js"]));
						} 

						TABS.autocontrol.comboRemove('User scripts', AUTO_SCRIPT.customName);
					}
					$('#scriptFileLabel').html(getMessage("rename") + ":");
					TABS.autocontrol.comboAdd('User scripts', newFileName, newFileName + '.js');
					AUTO_SCRIPT.customName = newFileName;
					$('select.dropdownScripts').val(newFileName);
				}
			});
			TABS.autocontrol.handleFileName.loaded = true;
		}else{
			$('input#scriptFileName').addClass('ui-block');
			$('p#scriptFileSaveStatus').html('<span style="color:orange">'+ getMessage("changesCanNotSavedUntilWorkingDirectorySet") +'</span>');	
			TABS.autocontrol.handleFileName.loaded = false;
		}
	}
}

//Called when the script has changed and needs to be saved
TABS.autocontrol.customScriptSave = function(customMessage){
	AUTO_SCRIPT.script = ace.edit("autocontrol-editor").getValue();
	if(!(AUTO_SCRIPT.customName==="")){
		$('p#scriptFileSaveStatus').html(getMessage('saving'));

		CONFIG.workingDirectory.getFile(AUTO_SCRIPT.customName + '.js', {create: true}, function(fileEntry) {
			// Create a FileWriter object for our FileEntry
			fileEntry.createWriter(function(fileWriter) {
				fileWriter.onwriteend = function() {
					if (fileWriter.length === 0 && AUTO_SCRIPT.script != "") {
						//fileWriter has been reset, write file
						// Create a new Blob and write it to log.txt.
						var blob = new Blob([AUTO_SCRIPT.script], {type: 'text/plain'});
						fileWriter.write(blob);
					} else {
						//file has been overwritten with blob
						if(customMessage)
							$('p#scriptFileSaveStatus').html(customMessage);
						else
							$('p#scriptFileSaveStatus').html(getMessage('allChangesSaved'));
							$('a.clone-button').show();
					}
				};
				fileWriter.onerror = TABS.autocontrol.fileWriteError;
				fileWriter.truncate(0);						
			}, TABS.autocontrol.fileWriteError);
		}, TABS.autocontrol.fileWriteError);
	}	
}

TABS.autocontrol.loadScriptEditor = function(startContent, editExisting){
	AUTO_SCRIPT.customName = "";
	TABS.autocontrol.loadBottomFromFiles("./tabs/autocontrol/customScript.html","./tabs/autocontrol/customScript.css",function(){

		TABS.autocontrol.handleFileName(true);

		$('a.clone-button').hide();
		$('a.edit-button').hide();
		
		if(editExisting){
			$('input#scriptFileName').val(AUTO_SCRIPT.name);
			$('p#scriptFileSaveStatus').html('');
			AUTO_SCRIPT.customName = AUTO_SCRIPT.name;
			$('#scriptFileLabel').html("Rename:");
		}else{
			$('select.dropdownScripts').val('?Custom');
			AUTO_SCRIPT.name = "Custom script";
		}

		var editor = ace.edit("autocontrol-editor");
		editor.setTheme("ace/theme/chrome");
		editor.setShowPrintMargin(false);
        
		editor.setWrapBehavioursEnabled(true);
		editor.getSession().setUseWrapMode(true);
		editor.$blockScrolling = Infinity;
		if(startContent){
			editor.setValue(startContent,-1);
		}else{
			editor.setValue('rcb.console.print("I am a simple script");\nrcb.endScript();',-1);	
		}
		editor.getSession().setMode("ace/mode/javascript");
        
		editor.on('change', function(e) {
			TABS.autocontrol.customScriptSave();
		});
		AUTO_SCRIPT.script = ace.edit("autocontrol-editor").getValue();
	});
}

//Loads custom content on the bottom of the scripting tab from files
TABS.autocontrol.loadBottomFromFiles = function (htmlFile, cssFile, callback){
	$('.tab-autocontrol div.lowerScriptContent').load(htmlFile,function(){
		translateAll();

		//Apply the css
		$("head").append($("<link rel='stylesheet' href='" + cssFile + "' type='text/css' media='screen' />"));
		if(callback) callback();
	});
}

//Loads custom content on the bottom of the scripting tab from text
TABS.autocontrol.loadBottomFromText = function (html, css, callback){
	$('div.lowerScriptContent').html(html);
	translateAll();
	$('html > head').append($('<style>' + css + '</style>'));
	if(callback) callback();
}

//When a file cannot be saved
TABS.autocontrol.fileWriteError = function(e){
	$('p#scriptFileSaveStatus').html('<span style="color:red">' + getMessage('errorAccessingFile') + ': ' + e.message + '</span>');
	console.log("File Save Error: " + e.message);
}

//When a file cannot be read
TABS.autocontrol.fileReadError = function(e){
	ace.edit("autocontrol-editor").setValue('',-1);
	$('#readOnlyMessage').html('<span style="color:red">' + getMessage('errorAccessingFile') + ': ' + e.message + '</span>')
	console.log("File Write Error: " + e.message);
}

//When an error occurs while scanning folder
TABS.autocontrol.fileError = function(e){
	console.log(getMessage('folderScanningError') + ": " + e.message);
}

//Adds an option under label
TABS.autocontrol.comboAdd = function(label, value, text) {
	//var def = $('select.dropdownScripts').val();
	//TABS.autocontrol.comboRemove(label, value, true); //avoid duplicates
    var translatedLabel = getMessage(label);
	if($('.tab-autocontrol select.dropdownScripts optGroup[label="' + translatedLabel + '"]').length===0){
		//Label does not exists, create it first
		$('.tab-autocontrol select.dropdownScripts').append('<optgroup label="' + translatedLabel + '"></optgroup>');
	}
	//Add the option, if it does not already exist
    if($('.tab-autocontrol select.dropdownScripts optGroup[label="' + translatedLabel + '"] option[value="' + value + '"]').length === 0){
      var i18n = "";  
      if(label !== 'User scripts'){
        i18n = ' i18n="'+ text +'"';
      }
      $('.tab-autocontrol select.dropdownScripts optGroup[label="' + translatedLabel + '"]').append('<option value="' + value + '"' + i18n + '>' + text + '</option>');
    }
	//$('select.dropdownScripts').val(def);
}

//Removes an option under label
TABS.autocontrol.comboRemove = function(label, value, noDelete) {
	if($('.tab-autocontrol select.dropdownScripts optGroup[label="' + label + '"]').length>0){
		$('.tab-autocontrol select.dropdownScripts optGroup[label="' + label + '"] option[value="' + value + '"]').remove();
	}
	if(!noDelete && $('.tab-autocontrol select.dropdownScripts optGroup[label="' + label + '"] option').length===0){
		$('.tab-autocontrol select.dropdownScripts optGroup[label="' + label + '"]').remove();
	}
}

TABS.autocontrol.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'autocontrol') {
        GUI.active_tab = 'autocontrol';
        googleAnalytics.sendAppView('autocontrol');
    }
    
    SCRIPT_ENVIRONMENT = 'autocontrol';

    if(callback) callback();    
};

TABS.autocontrol.cleanup = function (callback) {
    if (callback) callback();
};


var DATA_BACKUP = {};
initSandboxListeners();

function saveGUI(){
	DATA_BACKUP.OUTPUT_DATA = jQuery.extend(true, {}, OUTPUT_DATA);
	DATA_BACKUP.CONFIG = jQuery.extend(true, {}, CONFIG);
	DATA_BACKUP.USER_LIMITS = jQuery.extend(true, {}, USER_LIMITS);
	
	//Block UI
	//$('#left-pane').addClass('ui-block');
    $("#tabs > ul > li > a").addClass('ui-block'); //tabs
    $("#port-picker > ul > li:nth-child(2) > a").addClass('ui-block'); //connect button
    $("#left-pane-tare-button > a").addClass('ui-block'); //tare button
    $("#left-pane-tare-airspeed-button > a").addClass('ui-block'); //tare button
    $("#left-pane-tare-current-button > a").addClass('ui-block'); //tare button
    $(".tab-database .return-button").addClass('ui-block'); //database return button
    $("#port").addClass('ui-block'); //com port selector
	$('div.top').addClass('ui-block');
	$('#' + SCRIPT_ENVIRONMENT + '-start-button').text(getMessage('stopButton')).addClass('stop-state');
}

function restoreGUI(){
	OUTPUT_DATA = DATA_BACKUP.OUTPUT_DATA;
	CONFIG = DATA_BACKUP.CONFIG;
	USER_LIMITS = DATA_BACKUP.USER_LIMITS;
	//Unblock UI
	//$('#left-pane').removeClass('ui-block');
    $("#tabs > ul > li > a").removeClass('ui-block'); //tabs
    $("#port-picker > ul > li:nth-child(2) > a").removeClass('ui-block'); //connect button
    $("#left-pane-tare-button > a").removeClass('ui-block'); //tare button
    $("#left-pane-tare-airspeed-button > a").removeClass('ui-block'); //tare button
    $("#left-pane-tare-current-button > a").removeClass('ui-block'); //tare button
    $(".tab-database .return-button").removeClass('ui-block'); //database return button
    $("#port").removeClass('ui-block'); //com port selector
	$('div.top').removeClass('ui-block');
	$('#' + SCRIPT_ENVIRONMENT + '-start-button').text(getMessage('startButton')).removeClass('stop-state');
}

function stopScript(){
	var iframe = $('#sandboxFrame');
	var message = {
	   command: 'stop'
	};
	iframe[0].contentWindow.postMessage(message, '*');
    udp_clean();
	restoreGUI();	
}

function initSandboxListeners(){

	//Data received from sandbox
	window.addEventListener('message', function(event) {	
		if(SCRIPT_RUNNING){
			switch(event.data.command) {
			  case 'print':
				print(event.data.content);
				break;
			  case 'append':
				append(event.data.content);
				break;
			  case 'overwrite':
				overwrite(event.data.content);
				break;
			  case 'remove':
				remove(event.data.content);
				break;
			  case 'error':
				error(event.data.content);
				break;
              case 'clear':
				clearAutoConsole();
			  break;
			  case 'control':
				OUTPUT_DATA = event.data.content;
			  break;
			  case 'changeSafetyLimits':
				USER_LIMITS = event.data.content;
                refreshCutoffs();
			  break;
			  case 'endScript':
				endOfScript();
				break;
			  case 'newLogFile':
				newLogFile(event.data.content);
				break;
			  case 'newLogEntry':
				newLogEntry(event.data.content);
				break;
			  case 'newTextFile':
			    event.data.content.noHeader = true;
				newLogFile(event.data.content);
				break;
			  case 'appendTextFile':
			    event.data.content.rawText = true;
				newLogEntry(event.data.content);
				break;
			  case 'sensorsRead':
				sensorsRead(event.data.content);
				break;
			  case 'safetyCutoffDisable':
				safetyCutoffDisable(event.data.content);
			  break;
			  case 'debugModeEnable':
				debugModeEnable(event.data.content);
			  break;
			  case 'readOhm':
				readOhm();
			  break;
			  case 'tareLoadCells':
			  	tareLoadCells();
			  break;
              case 'tareCurrent':
			  	tareCurrent();
			  break;
			  case 'setPoles':
				setPoles(event.data.content);
			  break;
              case 'udpInit':
                udpInit(event.data.content);
              break;
              case 'udpSend':
                udpSend(event.data.content);
              break;
              case 'databaseAdd':
                databaseAdd(event.data.content);
              break;
              case 'databaseLog':
                databaseLog(event.data.content);  
              break;
              case 'databasePost':
                databasePost(event.data.content);
              break;
              case 'setControlProtocol':
                setControlProtocol(event.data.content);
              break;
			  default:
			    error("Unknown event (autocontrol.js): " + event.data.command);
			  break;
			}
		}
	});
  
    function udpInit(params){
        udp_init(params.LISTEN_PORT, params.OUT_IP, params.OUT_PORT);
    }
  
    function udpSend(params){
        udp_send(params.arrayBuffer);
    }
  
    function setControlProtocol(protocol){
      TABS.motors.controlBoardSet(protocol);
    }

	function newLogFile(params){
		logSample.newLog(params);
	}
	
	function newLogEntry(params){
		if(params.rawText){
			params.callback = function(result){
				scriptSendMessage('appendTextCallback',result);
			};
		}else{
			params.callback = function(result){
				scriptSendMessage('newLogEntryCallback',result);
			};
		}
		
		logSample.recordLine(params.data, params);
	}

	function tareLoadCells(){
		DATA.tareLoadCells(function(){
			scriptSendMessage('tareLoadCellsComplete');
		});
	}
  
    function tareCurrent(){
		DATA.tareCurrent(function(){
			scriptSendMessage('tareCurrentComplete');
		});
	}

	function sensorsRead(averageQty){
		logSample.getReadings(averageQty,function(result){
			scriptSendMessage('sensorReadCallback',result);
		});
	}

	function debugModeEnable(state){
		CONFIG.developper_mode = state;
	}

	function safetyCutoffDisable(state){
		CONFIG.safetyCutoffDisable = state;
		if(state)
			safetyCutoffActivated = false;
			userCutoff = false;
	}

	function setPoles(qty){
		CONFIG.numberOfMotorPoles.value = qty;
		refreshCutoffs();
		scriptSendMessage("updateSystemLimits",SYSTEM_LIMITS_15xx);
	}

	function readOhm(){
		readOhmmeter(function(val){
			scriptSendMessage("ohmUpdate",val);
		});
	}

	function print(params) {
		if(SCRIPT_RUNNING){
			$('div.wrapper', 'div#' + SCRIPT_ENVIRONMENT + '-console').append('<p id="' + params.id + '">>&nbsp;'+ params.message + '</p>');
			$('div#' + SCRIPT_ENVIRONMENT + '-console').scrollTop($('div.wrapper', 'div#' + SCRIPT_ENVIRONMENT + '-console').height());
            checkConsoleSize();
		}
	};

	function append(params) {
		if(SCRIPT_RUNNING){
			if(params.id===undefined){
				$('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p').last().append(params.message);
			}else{
				if(params.id>0){
					var id = "#"+params.id;
					$('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p' + id).last().append(params.message);
				} 
			}
            checkConsoleSize();
		}
	};
	
	function overwrite(params) {
		if(SCRIPT_RUNNING){
			if(params.id===undefined){
				$('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p').last().html('>&nbsp;' + params.message);
			}else{
				if(params.id>0){
					var id = "#"+params.id;
					$('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p' + id).last().html('>&nbsp;' + params.message);
				} 
			}
            checkConsoleSize();
		}
	};

	function remove(_id) {
		if(SCRIPT_RUNNING){
			if(_id===undefined){
				$('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p').last().remove();
			}else{
				if(_id>0){
					var id = "#"+_id;
					$('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p' + id).last().remove();
				} 
			}
            checkConsoleSize();
		}
	};

	function error(message) {
		if(SCRIPT_RUNNING){
			var console_log = $('div#' + SCRIPT_ENVIRONMENT + '-console');
			$('div.wrapper', console_log).append('<p style="color:red;">>&nbsp;'+ message + '</p>');
			console_log.scrollTop($('div.wrapper', console_log).height());
            checkConsoleSize();
		}
	};

	function endOfScript(){
        SCRIPT_RUNNING = false;
        restoreGUI(); //restores safety limits, throttle value, checked outputs, etc...
        udp_clean();
        checkConsoleSize();
    }
}

// UDP SOCKET
var udp = {};
function udp_init(LISTEN_PORT, OUT_IP, OUT_PORT){
  udp_clean();
  console.log("Listen port", LISTEN_PORT);
  console.log("OUT_IP", OUT_IP);
  console.log("OUT_PORT", OUT_PORT);
  udp.out_ip = OUT_IP;
  udp.out_port = OUT_PORT;
  
  // Create the Socket
  chrome.sockets.udp.create({}, function(socketInfo) {
    var socketId = socketInfo.socketId;
    udp.socketId = socketId;
    
    // Setup event handler and bind socket.
    chrome.sockets.udp.bind(socketId,
      "0.0.0.0", LISTEN_PORT, function(result) {
        if (result < 0) {
          scriptReportError("Error binding socket.");
          return;
        }
        scriptSendMessage('udpReady');
    });
  });
}

chrome.sockets.udp.onReceive.addListener(function listener(info) {
  if (info.socketId !== udp.socketId)
    return;
  console.log("received", info.data);
  scriptSendMessage('udpReceived', info.data);
});

function udp_send(arrayBuffer){
  chrome.sockets.udp.send(udp.socketId, arrayBuffer, udp.out_ip, udp.out_port, function(sendInfo) {
      if(sendInfo.resultCode<0){
        scriptReportError("Error sending UDP. Code: ", sendInfo.resultCode);
        return;
      }
      console.log("sent " + sendInfo.bytesSent);
      scriptSendMessage('udpSent');
  });
}

// stops the UDP sockets if any
function udp_clean(){
  if(udp.socketId){
    chrome.sockets.udp.close(udp.socketId, function(){
      console.log("Sucessfully closed the UDP socket.");
      delete udp.socketId;
    });
  }
}

function checkConsoleSize(){
  var elem = $("#longConsoleNotice");
  if($('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper p').length>500){
    elem.show();
  }else{
    elem.hide();
  }
}

function clearAutoConsole() {
  $('div#' + SCRIPT_ENVIRONMENT + '-console .wrapper').html("");
  checkConsoleSize();
};

function getSensorsForScript(){
	return {
		time: window.performance.now(),
		accX_g: DATA.getAccelerometer(0),
		accY_g: DATA.getAccelerometer(1),
		accZ_g: DATA.getAccelerometer(2),
		torque_Nm: DATA.getTorque(),
		thrust_kg: DATA.getThrust(),
		voltage: DATA.getESCVoltage(),
		current: DATA.getESCCurrent(),
		speed_rpm: DATA.getRPM(),
		loadCellLeft_mV: DATA.getLoadCellLeft(),
		loadCellRight_mV: DATA.getLoadCellRight(),
		loadCellThrust_mV: DATA.getLoadCellThrust(),
		debug0: SENSOR_DATA.debug[0],
		debug1: SENSOR_DATA.debug[1],
		debug2: SENSOR_DATA.debug[2],
		debug3: SENSOR_DATA.debug[3]
	};
}

function runScript(script, name){
    if(!SCRIPT_RUNNING){
        //Check the script has no syntax errors before running
        if(ace.edit(SCRIPT_ENVIRONMENT + "-editor").getSession().getAnnotations().length>0){
            $(' div.wrapper', $('div#' + SCRIPT_ENVIRONMENT + '-console')).html('<p style="color:red;">>Cannot run script containing syntax errors.</p>');
        }else{
            saveGUI();
            logSample.refreshHeader();
            $('div.wrapper', $('div#' + SCRIPT_ENVIRONMENT + '-console')).html('');

            googleAnalytics.sendEvent('ScriptRun', 'Click');

            SCRIPT_RUNNING = true;
            var iframe = $('#sandboxFrame');
            var message = {
               command: 'init',
               content: {
                  system: SYSTEM_LIMITS_15xx,
                  output: OUTPUT_DATA,
                  user: USER_LIMITS,
                  script: script,
                  name: name,
                  sensors: getSensorsForScript(),
                  config: {
                      boardId: CONFIG.boardId,
                      boardVersion: CONFIG.displayVersion,
                      firmwareVersion: CONFIG.firmwareVersion,
                      s1780detected: CONFIG.s1780detected,
                      controlBoard_flag_active: CONFIG.controlBoard_flag_active
                  },
                  CONTROL_PROTOCOLS: jQuery.extend(true, {}, CONTROL_PROTOCOLS)
              }
            };
            if(CONFIG.boardVersion === "Series 1780"){
              message.content.system = SYSTEM_LIMITS_1780;
            }
            iframe[0].contentWindow.postMessage(message, '*');	
            iframe[0].sockets = chrome.sockets;
        }
    }else stopScript();
}