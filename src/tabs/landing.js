'use strict';

TABS.landing = {};
TABS.landing.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'landing') {
        GUI.active_tab = 'landing';
        googleAnalytics.sendAppView('Landing');
    }

    $('#tabContent').load("./tabs/landing.html", function () {

        // translate to user-selected language
        translateAll();
        
		var msg = getMessage('currentVersion');
		
		var msg2 = getMessage('latestVersion');
        
        //used for the update button after user has pressed "Never Ask Again"
        $('.td1').append(msg + chrome.runtime.getManifest().version+"<br/>"); 
        if(CONFIG.latest_version.value === ''){
          //changeMemory(CONFIG.latest_version, chrome.runtime.getManifest().version); //for cases where the xml is not yet downloaded.
          console.log("XML file for latest app version was not downloaded.");
        }else{
          $('.td1').append(getMessage('latestVersion') + CONFIG.latest_version.value + "<br/>"); 
        }
        document.getElementById('updateSection').style.display = 'block';
              
        
        //checks if the internet is connected
            //if not, a "Could not reach server" is displayed
            //if yes, messages will be displayed according to current version
        var updateValidate = $('.updateValidate');
        /*$.ajax({
            url: 'https://www.google.ca/',
            success: function(data) {
                if(0 && chrome.runtime.getManifest().version ==  CONFIG.latest_version.value){
                    updateValidate.append("<p style='color:green;'>You have the latest version of the app</p>");
                }
                else{
                    document.getElementById('updateSection').style.display = 'block';                    
                }
            },
            error: function(data) {
                document.getElementById('updateSection').style.display = 'none';
                updateValidate.append("<p style='color:orange'>Could not reach server</p>");
            },
            timeout:6000
        });*/        
        
        
        //The process to follow after user has pressed update button
        //The updateNotify will be 1 if they press the update button
        $('#updateButton').click(function(){
          /* Disabled until time to properly debug the auto update code
            changeMemory(CONFIG.updateNotify, 1);
            $.getScript(".\\update\\update.js"); */
          
          // In the meantime, just open the software download page in our site
          window.open("https://docs.rcbenchmark.com/en/dynamometer/software/dynamometer-software-download.html", "_blank");
        });                
        
        
        // load changelog content
        $('div.changelog.configurator .wrapper').load('https://cdn-docs.rcbenchmark.com/software-release/rcb-thrust-stands-01/changelog.html');        

        $('.tab-landing #changeLog').on("click", function() {
                   
            $('#showDialog').load("https://cdn-docs.rcbenchmark.com/software-release/rcb-thrust-stands-01/changelog.html", function (response, status, xhr) {
              if(status == "error"){
                alert(getMessage('serverNA'));
              }else{
                $('#showDialog').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    title: "Change Log",
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 600,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
              } 
            });
            googleAnalytics.sendEvent('ChangeLog', 'Click');
        });
        
        // display tips and news section (if no server response just leave what there is already)
        var url = "https://docs.google.com/spreadsheets/d/119bzkEulpTHt1YVup_N2ZaVoYyWlxJQJBVvhbdl_ymI/pub?gid=0&single=true&output=csv";
        $.ajax(url).done(function displayNews(result){
            var data = CSVToArray(result);
            var display = [];
            var index;
            var totalItems = 4; // how many items to display
            
            function isExpired(item){
                var expireDate = new Date(item[1]);
                var nowDate = new Date();
                if(nowDate > expireDate){
                    return true;
                }
                return false;
            }
            
            function isImportant(item){
                var attr = item[0];
                if(attr === 'TRUE'){
                    return true;
                }
                return false;
            }
            
            function getEnglishMessage(item){
                return item[2];
            }

            function getUrl(item){
                return item[3];
            }
			
			function getChineseMessage(item){
                return item[4];
            }
            
            // get important items (must be displayed)
            var nonImportants = [];
            for (index = 1; index < data.length; ++index) {
                var item = data[index];
                if(!isExpired(item) && isImportant(item) && display.length < totalItems){
                    display.push(item);
                }
                
                // get non-important items to fill the list after
                if(!isExpired(item) && !isImportant(item)){
                    nonImportants.push(item);
                }
            }
            
            // randomly fill the rest
            while(display.length < totalItems && nonImportants.length > 0){
                var itemsLeft = nonImportants.length;
                var index = math.randomInt(itemsLeft); // returns a random integer
                var spliced = nonImportants.splice(index,1);
                display.push(spliced[0]);                
            }
            
            // display
            var ul = $('.newsSection');
            ul.empty();
            for (index = 0; index < display.length; ++index) {
                var item = display[index];
				
				  switch(localStorage.getItem("lang")) {
					case "en" :
					var message = getEnglishMessage(item);
					break
					case "zh" :
					var message = getChineseMessage(item);
					break
				default:
				var message = getEnglishMessage(item);
				} 
                var url = getUrl(item);
                
                /*
                <li>
                    <a class="link" href="https://store.rcbenchmark.com/" target="_blank">
                        store.rcbenchmark.com
                    </a>
                </li>
                */
                var listItem = '<li>'
                if(url){
                    listItem += '<a class="link" href="' + url + '" target="_blank" style = "text-decoration: underline;">';
                }
                listItem += message;
                if(url){
                    listItem += '</a>';
                }
                listItem += '</li>';
                ul.append(listItem);
            }
            
            
        }).error(function(){
          $("#loading-news").hide();
          var googleNA = getMessage('googleNA');
          $("#news-error").html('<span style="color:orange">' + googleNA + '</span>');
        });
        

        
        $('.tab-landing #credits').on('click', function() {          
            $('#showDialog').load("./credits.html", function () {
                $('#showDialog').dialog({
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "blind",
                        duration: 500
                    },
                    title: "Credits",
                    resizable: true,
                    draggable: true,
                    height: 600,
                    width: 600,
                    dialogClass: 'ui-dialog-osx',
                    modal: true,
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
                });
            });
            googleAnalytics.sendEvent('Credits', 'Click');
        });

        //Check if the GUI was updated with a new version
        chrome.storage.local.get('lastVersion', function(data){
            
            if(!(data.lastVersion === undefined)) {
                if (!(data.lastVersion === chrome.runtime.getManifest().version)) {
                    
                    var bounce = function(){
                        $("#changeLog").effect("bounce", "slow");  
                    };
                    
                    var repeatedBounce = function(times){
                        if(times>0){
                            bounce();
                            setTimeout(function(){
                                repeatedBounce(times-1);                   
                            }, 2000);
                        }
                    }
                    
                    $("#changeLog").html('<span style="color:green">*New* </span>Change Log');
                    repeatedBounce(4);
                    
                }
            }
            
            var obj = {};
            obj['lastVersion'] = chrome.runtime.getManifest().version;
            chrome.storage.local.set(obj);
        });

      var landing = $('#landing-right-button');
      var button = landing.find('.submit-feedback');
      button.on('click', function() {
            // Obtain a random number associated to the app install
            var property_name = 'random_number';
            var random_number;
            var submit = getMessage('submit');
            var message = $('#feedback').val();
            var email = $('#feedback-email').val();
            if(message !== ""){
                button.html(getMessage('sending'));
                chrome.storage.local.get(property_name, function(data){
                 // Generate number if running for the first time
                 if($.isEmptyObject(data))
                  {
                    random_number = Math.random()*10;
                    var obj = {};
                    obj[property_name] = random_number;
                    chrome.storage.local.set(obj);
                  }
                  else {
                    random_number = data.random_number;  
                  }
                    // Submit the comments in a google sheet
                    var formURL = "https://docs.google.com/forms/d/140lMvUhgniDF3l6Nal4Z9SnFmynRoPoNr0hqvOaVLPY/formResponse";
                    function clearFeedback(){
                      $('#feedback').val("");
                      $('#feedback-email').val("");
                      button.html(submit);
                    }
                    $.ajax({
                      type: "POST",
                      url: formURL,
                      data: { "entry.982187054": message + " " + email, 
                      "entry_1797468059": random_number},

                      success: function (data) { console.log("Message sent to developpers"); 
                        var received = getMessage('receivedthanks');
                        button.html('<span style="color:green">' + received + '</span>');
                        $('#feedback-error').html('');                          
                        setTimeout(clearFeedback, 3000);
                      },
                      error: function (jqXHR, textStatus) { 
                        console.log("Message not sent. There might be a problem with your internet connection"); 
                        var googleNA = getMessage('googleNA');
                        $('#feedback-error').html('<span style="color:orange">' + googleNA + '</span>');
                        clearFeedback();
                      },
                    });
                  });
            } 
        });
        $('#landing-left a, #landing-right a, .ui-dialog-osx a').click(function () {
            googleAnalytics.sendEvent('ExternalUrls', 'Click', $(this).prop('href'));
        });
        
        

        if (callback) callback();
    });
};

TABS.landing.cleanup = function (callback) {
    if (callback) callback();
};