'use strict';

var SCALES = { //define scales for the up/down arrows to adjust cutoff values
    voltage: 0.1,			
    current: 0.5,
    currentBurst: 0.5,
    power: 10,
    thrust: 0.1,			
    torque: 0.1,		
    motorSpeed: 10,
    vibration: 0.1,
    temperature: 1
};

//Variables for the safety cutoff
var safetyCutoffActivated = false;
var lastCutoff = '';
var userCutoff = false;
var wasSafetyCutoffActivated = false; //used to determine when it is appropriate to notify the logging (ie only once)
function safetyLimits(){
    var logFileCutoffTxt = "";
    
	//Check safety limits
	var exceeding = [];
	var exceeded = []; 
	if(!CONFIG.safetyCutoffDisable){
        
        // makes the text glow orange if its limit is exceeded
        function highlight(state, elemName) {
            if(state){
                $('#data-pane #data dt.' + elemName).css('color', '#ff9201');
            }
        }
        
        // checks the limits
        // valFct can also be a direct value (not a function)
        // custom
        function checkLimit(id, valFct, min, max, customLabel){
            var val = valFct;
            if (typeof valFct === "function") {
                val = valFct();
            }
            if(val < min || val > max){
                if(customLabel){
                    exceeded.push(customLabel);
                    if(exceeding.indexOf(customLabel) === -1){
                        exceeding.push(customLabel);
                    }
                }else{
                    exceeded.push(id);
                    if(exceeding.indexOf(id) === -1){
                        exceeding.push(id);
                    }
                }
                
                highlight(true, id);
                
                var exceedVal;
                if(val < min){
                    exceedVal = min;
                }else{
                    exceedVal = max;
                }
                logFileCutoffTxt = "Cutoff: " + id + " (val=" + val + ", limit=" + exceedVal + ")";
            }else{
                highlight(false, id);
            }
        }
      
        var sideID = '';
        if(CONFIG.s1780detected.Bside){
          sideID = 'A'; 
        }
      
        if(CONFIG.boardVersion === "Series 1780"){
          checkLimit('voltage' + sideID, DATA.getESCVoltage, USER_LIMITS.voltageMinA, USER_LIMITS.voltageMaxA);
          checkLimit('currentBurst' + sideID, DATA.getESCCurrent, USER_LIMITS.currentBurstMin, USER_LIMITS.currentBurstMax);
          checkLimit('current' + sideID, DATA.getESCCurrentBurst, USER_LIMITS.currentMin, USER_LIMITS.currentMax);
          checkLimit('power' + sideID, DATA.getElectricalPower, USER_LIMITS.powerMinA, USER_LIMITS.powerMaxA);
          checkLimit('thrust' + sideID, DATA.getThrust, USER_LIMITS.thrustMin, USER_LIMITS.thrustMax);
          checkLimit('torque' + sideID, DATA.getTorque, USER_LIMITS.torqueMin, USER_LIMITS.torqueMax);
        }else{
          checkLimit('voltage' + sideID, DATA.getESCVoltage, USER_LIMITS.voltageMin, USER_LIMITS.voltageMax);
          checkLimit('currentBurst' + sideID, DATA.getESCCurrent, USER_LIMITS.currentBurstMin, USER_LIMITS.currentBurstMax);
          checkLimit('current' + sideID, DATA.getESCCurrentBurst, USER_LIMITS.currentMin, USER_LIMITS.currentMax);
          checkLimit('power' + sideID, DATA.getElectricalPower, USER_LIMITS.powerMin, USER_LIMITS.powerMax);
          checkLimit('thrust' + sideID, DATA.getThrust, USER_LIMITS.thrustMin, USER_LIMITS.thrustMax);
          checkLimit('torque' + sideID, DATA.getTorque, USER_LIMITS.torqueMin, USER_LIMITS.torqueMax);
        }
      
        switch(CONFIG.mainRPMsensor.value) {
            case "electrical":
                checkLimit('rpmElectrical', DATA.getRPM, USER_LIMITS.rpmMin, USER_LIMITS.rpmMax);
                break;
            case "optical":   
                checkLimit('rpmOptical' + sideID, DATA.getRPM, USER_LIMITS.rpmMin, USER_LIMITS.rpmMax);
                break;
            default:
                console.error("Unknown rpm sensor type");
        }
      
        //1780 B side
        if(CONFIG.s1780detected.Bside){
          checkLimit('torqueB', function(){return DATA.getTorque(true)}, USER_LIMITS.torqueMinB, USER_LIMITS.torqueMaxB);
          checkLimit('thrustB', function(){return DATA.getThrust(true)}, USER_LIMITS.thrustMinB, USER_LIMITS.thrustMaxB);
          checkLimit('rpmOpticalB', function(){return DATA.getRPM(true)}, USER_LIMITS.rpmMinB, USER_LIMITS.rpmMaxB);
          checkLimit('powerB', function(){return DATA.getElectricalPower(true)}, USER_LIMITS.powerMinB, USER_LIMITS.powerMaxB);
          checkLimit('currentB', function(){return DATA.getESCCurrentBurst(true)}, USER_LIMITS.currentMinB, USER_LIMITS.currentMaxB);
          checkLimit('currentBurstB', function(){return DATA.getESCCurrent(true)}, USER_LIMITS.currentBurstMinB, USER_LIMITS.currentBurstMaxB);
          checkLimit('voltageB', function(){return DATA.getESCVoltage(true)}, USER_LIMITS.voltageMinB, USER_LIMITS.voltageMaxB);
        }
      
        if(CONFIG.boardVersion === "Series 1780"){
          if(!(HACKS && HACKS.s1780 && HACKS.s1780.disableLoadCellOverloadProtection)){
            checkLimit('loadCellOverload', SENSOR_DATA.s1780loadCellOverload, 0, 0, 'loadcelloverload');
          }
          checkLimit('limitSwitch', SENSOR_DATA.s1780limitSwitch, 0, 0, 'limitSwitch');
        }
        
        if(CONFIG.boardVersion === "Series 1580")
		  checkLimit('vibration', SENSOR_DATA.vibration, USER_LIMITS.vibrationMin, USER_LIMITS.vibrationMax);
        
        var temperatures = DATA.getTemperatures();
        for(var i=0; i<temperatures.length; i++){
            var temp = temperatures[i];
            var id = temp.id;
            var label = temp.label;
            var textId = "temp" + id;
            
            var userMin = USER_LIMITS.temperaturesMin[id];
            var userMax = USER_LIMITS.temperaturesMax[id];
            var exceedLabel = "temp";
            checkLimit(textId, temp.value, userMin, userMax, exceedLabel);
        }; 
        
		if(exceeded.length != 0 || userCutoff) safetyCutoffActivated = true;
	}else{
		safetyCutoffActivated = false;
	}

	//Generate the text explaining what limits are exceeded
	var exceededText = '';
	var limitsExceededText = '';
	if(exceeding.length > 0){
		exceededText = '*** ';
		if(exceeding.length != 0){
			for (var i = 0; i < exceeding.length; i++){
                var limit_name = getMessage('limit_' + exceeding[i]);
                if(i==0){
                  limit_name = limit_name.capitalizeFirstLetter();
                }
				if(i == exceeding.length - 1)
					limitsExceededText += limit_name;
				else if(i == exceeding.length - 2)
					limitsExceededText += limit_name + getMessage("and");
				else
					limitsExceededText += limit_name + ', ';
			}
			exceededText += limitsExceededText;
		}
		exceededText += " "
		if(exceeding.length > 1){
          exceededText += getMessage("safetyLimits");
        }else{
          exceededText += getMessage("safetyLimit");
        }
        exceededText += ' ***';
      
        // only update the lastCutoff text if actually triggerring a cutoff (prevents saying the cutoff was because of voltage, when in reality it was something else. Voltage spike occured when motor suddenly stopped).
        if(safetyCutoffActivated && !wasSafetyCutoffActivated){
          lastCutoff = "(" + limitsExceededText.uncapitalizeFirstLetter() + ") ";
        }
	}

	//Handle the cutoff autoreset
	var autoReset;
    if(CONFIG.boardVersion === "Series 1780"){
      autoReset = exceeded.length===0 && (!OUTPUT_DATA.active[0] && OUTPUT_DATA.ESCA === CONFIG.ESCCutoffA.value);
      if(CONFIG.s1780detected.Bside){
        autoReset = autoReset && (!OUTPUT_DATA.active[2] && OUTPUT_DATA.ESCB === CONFIG.ESCCutoffB.value);
      }
    }else{
      autoReset = exceeded.length===0 && (!OUTPUT_DATA.active[0] && OUTPUT_DATA.ESC_PWM === CONFIG.ESCCutoff.value);
    }
    
	if(autoReset) {
		safetyCutoffActivated = false;
		userCutoff = false;
	}
  
    //Handle the cutoff
	if(safetyCutoffActivated){
		$('#content div.sliders :eq( 0 )').prop('disabled', true);
		$('#content div.sliders :eq( 0 )').css('opacity', '0.5');
        if(CONFIG.boardVersion === "Series 1780"){
          $('#content div.sliders :eq( 2 )').prop('disabled', true);
          $('#content div.sliders :eq( 2 )').css('opacity', '0.5');
          $('#content div.sliders :eq( 0 )').val(CONFIG.ESCCutoffA.value);
          $('#content div.values li:eq( 0 )').text(CONFIG.ESCCutoffA.value);
          $('#content div.sliders :eq( 2 )').val(CONFIG.ESCCutoffB.value);
          $('#content div.values li:eq( 2 )').text(CONFIG.ESCCutoffB.value);
        }else{
          $('#content div.sliders :eq( 0 )').val(CONFIG.ESCCutoff.value);
		  $('#content div.values li:first').text(CONFIG.ESCCutoff.value);
        }
		$('#content .tab-motors p.cutoff').show();
		if(exceeding.length > 0){
			$('#content .tab-motors p.cutoff').html(exceededText);	
			if(OUTPUT_DATA.active[0]) scriptReportError(exceededText);
		}else{
			if(userCutoff){
				$('#content .tab-motors p.cutoff').html(getMessage("SPACEBARwasPressedToCutMotors"));
				$('#data-pane #data dt.cutoff').html(getMessage("SPACEBARwasPressed"));
				if(OUTPUT_DATA.active[0]) scriptReportError(getMessage("spacebarwasPressed"));
			}else{
				$('#content .tab-motors p.cutoff').html(getMessage("cutOffinCutOffJsfile") + lastCutoff + getMessage("wasActivatedRecheckESCSlidertoReset"));	
				$('#data-pane #data dt.cutoff').html(exceededText);
			}
		}
		$('#data-pane #data dt.cutoff').show();
		OUTPUT_DATA.ESC_PWM = CONFIG.ESCCutoff.value;
        OUTPUT_DATA.ESCA = CONFIG.ESCCutoffA.value;
        OUTPUT_DATA.ESCB = CONFIG.ESCCutoffB.value;
	} else {
		$('#content div.sliders :eq( 0 )').prop('disabled', false);
		$('#content div.sliders :eq( 0 )').css('opacity', '1');
        $('#content div.sliders :eq( 2 )').prop('disabled', false);
		$('#content div.sliders :eq( 2 )').css('opacity', '1');
		$('#data-pane #data dt.cutoff').hide();
		$('#content .tab-motors p.cutoff').hide();
	}
    
    //handle notification to the log file
    if(safetyCutoffActivated){
        if(!wasSafetyCutoffActivated){
            if(userCutoff){
                logSample.logAppMessage("Cutoff: spacebar was pressed");
                GUI.log("Cutoff: spacebar was pressed");
                console.log("Cutoff: spacebar was pressed");
            }else{
                logSample.logAppMessage(logFileCutoffTxt);
                GUI.log(logFileCutoffTxt);
                console.log(logFileCutoffTxt);
            }
        }
    }
    
    wasSafetyCutoffActivated = safetyCutoffActivated;
}

function currentCableWarning(){
  if(USER_LIMITS.currentMax>20 || (CONFIG.s1780detected.Bside && USER_LIMITS.currentMaxB>20)){
      $("img#cablesWarning").show();
  }else{
      $("img#cablesWarning").hide();
  }
}

function refreshCutoffs(){    
    //overwrite default 1780 limits with any available extended limits
    var MERGED_LIMITS_1780 = $.extend(true, {}, SYSTEM_LIMITS_1780);
    for (var key in SYSTEM_LIMITS_1780){
      if(SYSTEM_LIMITS_1780.extended !== undefined && SYSTEM_LIMITS_1780.extended[key] !== undefined){
        MERGED_LIMITS_1780[key] = SYSTEM_LIMITS_1780.extended[key]; 
      }
    }
    
    //copy the 15xx limits before applying the hack codes
    var MERGED_LIMITS_15xx = $.extend(true, {}, SYSTEM_LIMITS_15xx);
      
    //merge hack codes if any
    if(HACKS !== undefined){
      if(HACKS.s1780 !== undefined){
        for (var key in HACKS.s1780){
          if(MERGED_LIMITS_1780[key] !== undefined && math.abs(HACKS.s1780[key]) > math.abs(MERGED_LIMITS_1780[key])){
            MERGED_LIMITS_1780[key] = HACKS.s1780[key];
          }
        }
      }
      if(HACKS.s15xx !== undefined){
        for (var key in HACKS.s15xx){
          if(MERGED_LIMITS_15xx[key] !== undefined && math.abs(HACKS.s15xx[key]) > math.abs(MERGED_LIMITS_15xx[key])){
            MERGED_LIMITS_15xx[key] = HACKS.s15xx[key];
          }
        }
      }
    }
  
    /*//replace all undefined or NaN by 0
    for (var key in MERGED_LIMITS_15xx){
      if(isNaN(MERGED_LIMITS_15xx[key]) || MERGED_LIMITS_15xx[key]===undefined){
        MERGED_LIMITS_15xx[key] = 0;
      }
    }
    for (var key in MERGED_LIMITS_1780){
      if(isNaN(MERGED_LIMITS_1780[key]) || MERGED_LIMITS_1780[key]===undefined){
        MERGED_LIMITS_1780[key] = 0;
      }
    }*/
  
    var torqueDecimals = ($.inArray(UNITS.display['torque'], ["lbfft", "kgfm"]) > -1) ? 2 : 1;
  
    currentCableWarning();   
	var temperatures = DATA.getTemperatures();
    
    //RPM sensor system limit
    var max_eRPM_15xx = MERGED_LIMITS_15xx.erpmMax || 500000; //in case of hack code used to increase this limit.
    var max_eRPM_1780 = MERGED_LIMITS_1780.erpmMax || 500000; //in case of hack code used to increase this limit.
    switch(CONFIG.mainRPMsensor.value) {
        case 'electrical':
            MERGED_LIMITS_15xx.rpmMax = Math.floor(2*max_eRPM_15xx/CONFIG.numberOfMotorPoles.value / 1000)*1000;
            break;
        case 'optical':
            MERGED_LIMITS_15xx.rpmMax = Math.floor(2*max_eRPM_15xx/CONFIG.numberOfOpticalTape.value / 1000)*1000;
            MERGED_LIMITS_1780.rpmMax = Math.floor(2*max_eRPM_1780/CONFIG.numberOfOpticalTape.value / 1000)*1000;
            MERGED_LIMITS_1780.rpmMaxB = Math.floor(2*max_eRPM_1780/CONFIG.numberOfOpticalTapeB.value / 1000)*1000;
            break;
        default:
            throw "Sensor not recognized"
    }
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
      //Clip user limits if above system limits (needed because of hack codes)
      if(USER_LIMITS.voltageMax > MERGED_LIMITS_15xx.voltageMax) USER_LIMITS.voltageMax = MERGED_LIMITS_15xx.voltageMax;
      if(USER_LIMITS.currentMax > MERGED_LIMITS_15xx.currentMax) USER_LIMITS.currentMax = MERGED_LIMITS_15xx.currentMax;
      if(USER_LIMITS.currentMin < MERGED_LIMITS_15xx.currentMin) USER_LIMITS.currentMin = MERGED_LIMITS_15xx.currentMin;
      if(USER_LIMITS.currentBurstMax > MERGED_LIMITS_15xx.currentBurstMax) USER_LIMITS.currentBurstMax = MERGED_LIMITS_15xx.currentBurstMax;
      if(USER_LIMITS.currentBurstMin < MERGED_LIMITS_15xx.currentBurstMin) USER_LIMITS.currentBurstMin = MERGED_LIMITS_15xx.currentBurstMin;
      if(USER_LIMITS.powerMax > MERGED_LIMITS_15xx.powerMax) USER_LIMITS.powerMax = MERGED_LIMITS_15xx.powerMax;
      if(USER_LIMITS.rpmMax > MERGED_LIMITS_15xx.rpmMax) USER_LIMITS.rpmMax = MERGED_LIMITS_15xx.rpmMax;
      if(USER_LIMITS.thrustMax > MERGED_LIMITS_15xx.thrustMax) USER_LIMITS.thrustMax = MERGED_LIMITS_15xx.thrustMax;
      if(USER_LIMITS.thrustMin < MERGED_LIMITS_15xx.thrustMin) USER_LIMITS.thrustMin = MERGED_LIMITS_15xx.thrustMin;
      if(USER_LIMITS.torqueMin < MERGED_LIMITS_15xx.torqueMin) USER_LIMITS.torqueMin = MERGED_LIMITS_15xx.torqueMin;
      if(USER_LIMITS.torqueMax > MERGED_LIMITS_15xx.torqueMax) USER_LIMITS.torqueMax = MERGED_LIMITS_15xx.torqueMax;
      if(USER_LIMITS.vibrationMax > MERGED_LIMITS_15xx.vibrationMax) USER_LIMITS.vibrationMax = MERGED_LIMITS_15xx.vibrationMax;
    } 
    for(var i=0; i<temperatures.length; i++){
        var temp = temperatures[i];

        var id = temp.id;
        var sysMax = MERGED_LIMITS_15xx.temperatureMax;
        var sysMin = MERGED_LIMITS_15xx.temperatureMin;
        var userMin = USER_LIMITS.temperaturesMin[id];
        var userMax = USER_LIMITS.temperaturesMax[id];

        // assign default if never set
        if(userMin === undefined || userMax === undefined){
            userMin = USER_LIMITS.temperatureMin;
            userMax = USER_LIMITS.temperatureMax;
        }

        // check range
        if(userMin < sysMin) userMin = sysMin;
        if(userMax > sysMax) userMax = sysMax;

        // write back user limits
        USER_LIMITS.temperaturesMin[id] = userMin;
        USER_LIMITS.temperaturesMax[id] = userMax;
    };  
    if(CONFIG.boardVersion === "Series 1780"){
      // A side
      if(USER_LIMITS.voltageMaxA > MERGED_LIMITS_1780.voltageMaxA) USER_LIMITS.voltageMaxA = MERGED_LIMITS_1780.voltageMaxA;
      if(USER_LIMITS.currentMax > MERGED_LIMITS_1780.currentMaxA) USER_LIMITS.currentMax = MERGED_LIMITS_1780.currentMaxA;
      if(USER_LIMITS.currentBurstMax > MERGED_LIMITS_1780.currentBurstMaxA) USER_LIMITS.currentBurstMax = MERGED_LIMITS_1780.currentBurstMaxA;
      if(USER_LIMITS.currentMin < MERGED_LIMITS_1780.currentMinA) USER_LIMITS.currentMin = MERGED_LIMITS_1780.currentMinA;
      if(USER_LIMITS.currentBurstMin < MERGED_LIMITS_1780.currentBurstMinA) USER_LIMITS.currentBurstMin = MERGED_LIMITS_1780.currentBurstMinA;
      if(USER_LIMITS.powerMaxA > MERGED_LIMITS_1780.powerMaxA) USER_LIMITS.powerMaxA = MERGED_LIMITS_1780.powerMaxA;
      if(USER_LIMITS.rpmMax > MERGED_LIMITS_1780.rpmMax) USER_LIMITS.rpmMax = MERGED_LIMITS_1780.rpmMax;
      if(USER_LIMITS.thrustMax > MERGED_LIMITS_1780.thrustMaxA) USER_LIMITS.thrustMax = MERGED_LIMITS_1780.thrustMaxA;
      if(USER_LIMITS.thrustMin < MERGED_LIMITS_1780.thrustMinA) USER_LIMITS.thrustMin = MERGED_LIMITS_1780.thrustMinA;
      if(USER_LIMITS.torqueMin < MERGED_LIMITS_1780.torqueMinA) USER_LIMITS.torqueMin = MERGED_LIMITS_1780.torqueMinA;
      if(USER_LIMITS.torqueMax > MERGED_LIMITS_1780.torqueMaxA) USER_LIMITS.torqueMax = MERGED_LIMITS_1780.torqueMaxA;
      
      // B side
      if(USER_LIMITS.voltageMaxB > MERGED_LIMITS_1780.voltageMaxB) USER_LIMITS.voltageMaxB = MERGED_LIMITS_1780.voltageMaxB;
      if(USER_LIMITS.currentMaxB > MERGED_LIMITS_1780.currentMaxB) USER_LIMITS.currentMaxB = MERGED_LIMITS_1780.currentMaxB;
      if(USER_LIMITS.currentBurstMaxB > MERGED_LIMITS_1780.currentBurstMaxB) USER_LIMITS.currentBurstMaxB = MERGED_LIMITS_1780.currentBurstMaxB;
      if(USER_LIMITS.currentMinB < MERGED_LIMITS_1780.currentMinB) USER_LIMITS.currentMinB = MERGED_LIMITS_1780.currentMinB;
      if(USER_LIMITS.currentBurstMinB < MERGED_LIMITS_1780.currentBurstMinB) USER_LIMITS.currentBurstMinB = MERGED_LIMITS_1780.currentBurstMinB;
      if(USER_LIMITS.powerMaxB > MERGED_LIMITS_1780.powerMaxB) USER_LIMITS.powerMaxB = MERGED_LIMITS_1780.powerMaxB;
      if(USER_LIMITS.rpmMaxB > MERGED_LIMITS_1780.rpmMaxB) USER_LIMITS.rpmMaxB = MERGED_LIMITS_1780.rpmMaxB;
      if(USER_LIMITS.thrustMaxB > MERGED_LIMITS_1780.thrustMaxB) USER_LIMITS.thrustMaxB = MERGED_LIMITS_1780.thrustMaxB;
      if(USER_LIMITS.thrustMinB < MERGED_LIMITS_1780.thrustMinB) USER_LIMITS.thrustMinB = MERGED_LIMITS_1780.thrustMinB;
      if(USER_LIMITS.torqueMinB < MERGED_LIMITS_1780.torqueMinB) USER_LIMITS.torqueMinB = MERGED_LIMITS_1780.torqueMinB;
      if(USER_LIMITS.torqueMaxB > MERGED_LIMITS_1780.torqueMaxB) USER_LIMITS.torqueMaxB = MERGED_LIMITS_1780.torqueMaxB;
    }
	
    //GUI cutoffs management
  
    if(CONFIG.boardVersion === "Series 1780"){
      if(CONFIG.s1780detected.Bside){
        $("#cutoffValueMessage").html(getMessage("cutoffValuesMessageAB", [TABS.motors.pwmPretty(CONFIG.ESCCutoff.value)+'/'+TABS.motors.pwmPretty(CONFIG.ESCCutoffB.value)])); 
      }else{
        $("#cutoffValueMessage").html(getMessage("cutoffValueMessage", [TABS.motors.pwmPretty(CONFIG.ESCCutoff.value)])); 
      }
    }else{
      $("#cutoffValueMessage").html(getMessage("cutoffValueMessage", [TABS.motors.pwmPretty(CONFIG.ESCCutoff.value)]));
    }
  
	//display user units
	$('.tab-cutoffs #thrustUnit').html('<span class="1780Bside">' + getMessage("sideA") + ': </span>' + getMessage("cutoffThrust") + ' (' + UNITS.text[UNITS.display['thrust']] + ')');
	$('.tab-cutoffs #torqueUnit').html('<span class="1780Bside">' + getMessage("sideA") + ': </span>' + getMessage("cutoffTorque") + '(' + UNITS.text[UNITS.display['torque']] + ')');
	$('.tab-cutoffs #motorSpeedUnit').html('<span class="1780Bside">' + getMessage("sideA") + ': </span>' + getMessage("cutoffRPM") +'(' + UNITS.text[UNITS.display['motorSpeed']] + ')');
  	$('.tab-cutoffs #thrustUnitB').html('' + getMessage("sideB") + ': ' + getMessage("cutoffThrust") + ' (' + UNITS.text[UNITS.display['thrust']] + ')');
	$('.tab-cutoffs #torqueUnitB').html('' + getMessage("sideB") + ': ' + getMessage("cutoffTorque") + ' (' + UNITS.text[UNITS.display['torque']] + ')');
	$('.tab-cutoffs #motorSpeedUnitB').html('' + getMessage("sideB") + ': ' + getMessage("cutoffRPM") +' (' + UNITS.text[UNITS.display['motorSpeed']] + ')');
    for(var i=0; i<temperatures.length; i++){   
        var temp = temperatures[i];
        var id = temp.id;
        var label = temp.label;
        $('.tab-cutoffs #temperatureUnit' + (i+1)).html(getMessage("temp") + ' ' + label +' (' + UNITS.text[UNITS.display['temperature']] + ')');
    }
    
	//variables
	var voltageMinGUI = $('.tab-cutoffs #voltageMin');
	var voltageMaxGUI = $('.tab-cutoffs #voltageMax');
	var currentMinGUI = $('.tab-cutoffs #currentMin');
	var currentMaxGUI = $('.tab-cutoffs #currentMax');
    var currentBurstMinGUI = $('.tab-cutoffs #currentBurstMin');
	var currentBurstMaxGUI = $('.tab-cutoffs #currentBurstMax');
    var powerMinGUI = $('.tab-cutoffs #powerMin');
	var powerMaxGUI = $('.tab-cutoffs #powerMax');
	var thrustMinGUI = $('.tab-cutoffs #thrustMin');
	var thrustMaxGUI = $('.tab-cutoffs #thrustMax');
	var torqueMinGUI = $('.tab-cutoffs #torqueMin');
	var torqueMaxGUI = $('.tab-cutoffs #torqueMax');
	var rpmMinGUI = $('.tab-cutoffs #rpmMin');
	var rpmMaxGUI = $('.tab-cutoffs #rpmMax');
  
  	var voltageMinGUIB = $('.tab-cutoffs #voltageMinB');
	var voltageMaxGUIB = $('.tab-cutoffs #voltageMaxB');
	var currentMinGUIB = $('.tab-cutoffs #currentMinB');
	var currentMaxGUIB = $('.tab-cutoffs #currentMaxB');
    var currentBurstMinGUIB = $('.tab-cutoffs #currentBurstMinB');
	var currentBurstMaxGUIB = $('.tab-cutoffs #currentBurstMaxB');
    var powerMinGUIB = $('.tab-cutoffs #powerMinB');
	var powerMaxGUIB = $('.tab-cutoffs #powerMaxB');
	var thrustMinGUIB = $('.tab-cutoffs #thrustMinB');
	var thrustMaxGUIB = $('.tab-cutoffs #thrustMaxB');
	var torqueMinGUIB = $('.tab-cutoffs #torqueMinB');
	var torqueMaxGUIB = $('.tab-cutoffs #torqueMaxB');
	var rpmMinGUIB = $('.tab-cutoffs #rpmMinB');
	var rpmMaxGUIB = $('.tab-cutoffs #rpmMaxB');
  
    var vibrationMinGUI = $('.tab-cutoffs #vibrationMin');
	var vibrationMaxGUI = $('.tab-cutoffs #vibrationMax');
    var tempMinGUI = [];
    var tempMaxGUI = [];
    for(var i=0; i<temperatures.length; i++){   
        var temp = temperatures[i];
        var id = temp.id;
        tempMinGUI[i] = $('.tab-cutoffs #temp' + (i+1) + 'Min');
        tempMaxGUI[i] = $('.tab-cutoffs #temp' + (i+1) + 'Max');
    }
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1520"){
      //min voltage
      voltageMinGUI.attr({
          step: SCALES.voltage,
          min: MERGED_LIMITS_15xx.voltageMin,
          max: USER_LIMITS.voltageMax
      });
      voltageMinGUI.val(USER_LIMITS.voltageMin);
      voltageMinGUI.change(function () {
          minLimitsGUI(voltageMinGUI, voltageMaxGUI);
      });
      //max voltage
      voltageMaxGUI.attr({
          step: SCALES.voltage,
          min: USER_LIMITS.voltageMin,
          max: MERGED_LIMITS_15xx.voltageMax
      });
      voltageMaxGUI.val(USER_LIMITS.voltageMax);
      voltageMaxGUI.change(function () {
          maxLimitsGUI(voltageMinGUI, voltageMaxGUI);
      });
      //system voltage
      $('.tab-cutoffs #systemVoltage').html(MERGED_LIMITS_15xx.voltageMin + ' / ' + MERGED_LIMITS_15xx.voltageMax);

      //min current
      currentMinGUI.attr({
          step: SCALES.current,
          min: MERGED_LIMITS_15xx.currentMin,
          max: USER_LIMITS.currentMax
      });
      currentMinGUI.val(USER_LIMITS.currentMin);
      currentMinGUI.change(function () {
          minLimitsGUI(currentMinGUI, currentMaxGUI);
      });
      //max current
      currentMaxGUI.attr({
          step: SCALES.current,
          min: USER_LIMITS.currentMin,
          max: MERGED_LIMITS_15xx.currentMax
      });
      currentMaxGUI.val(USER_LIMITS.currentMax);
      currentCableWarning();
      currentMaxGUI.change(function () {
          maxLimitsGUI(currentMinGUI, currentMaxGUI);
          currentCableWarning();
      });
      //system current
      $('.tab-cutoffs #systemCurrent').html(MERGED_LIMITS_15xx.currentMin + ' / ' + MERGED_LIMITS_15xx.currentMax);

      //min currentBurst
      currentBurstMinGUI.attr({
          step: SCALES.currentBurst,
          min: MERGED_LIMITS_15xx.currentBurstMin,
          max: USER_LIMITS.currentBurstMax
      });
      currentBurstMinGUI.val(USER_LIMITS.currentBurstMin);
      currentBurstMinGUI.change(function () {
          minLimitsGUI(currentBurstMinGUI, currentBurstMaxGUI);
      });
      //max currentBurst
      currentBurstMaxGUI.attr({
          step: SCALES.currentBurst,
          min: USER_LIMITS.currentBurstMin,
          max: MERGED_LIMITS_15xx.currentBurstMax
      });
      currentBurstMaxGUI.val(USER_LIMITS.currentBurstMax);
      currentBurstMaxGUI.change(function () {
          maxLimitsGUI(currentBurstMinGUI, currentBurstMaxGUI);
      });
      //system currentBurst
      $('.tab-cutoffs #systemCurrentBurst').html(MERGED_LIMITS_15xx.currentBurstMin + ' / ' + MERGED_LIMITS_15xx.currentBurstMax);

      //min power
      powerMinGUI.attr({
          step: SCALES.power,
          min: MERGED_LIMITS_15xx.powerMin,
          max: USER_LIMITS.powerMax
      });
      powerMinGUI.val(USER_LIMITS.powerMin);
      powerMinGUI.change(function () {
          minLimitsGUI(powerMinGUI, powerMaxGUI);
      });
      //max power
      powerMaxGUI.attr({
          step: SCALES.power,
          min: USER_LIMITS.powerMin,
          max: MERGED_LIMITS_15xx.powerMax
      });
      powerMaxGUI.val(USER_LIMITS.powerMax);
      powerMaxGUI.change(function () {
          maxLimitsGUI(powerMinGUI, powerMaxGUI);
      });
      //system power
      $('.tab-cutoffs #systemPower').html(MERGED_LIMITS_15xx.powerMin + ' / ' + MERGED_LIMITS_15xx.powerMax);   

      //min thrust
      thrustMinGUI.attr({
          step: convertScale('thrust', SCALES.thrust),
          min: UNITS.convertToDisplay('thrust', MERGED_LIMITS_15xx.thrustMin).toFixed(0),
          max: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMax).toFixed(1)
      });
      thrustMinGUI.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMin).toFixed(1));
      thrustMinGUI.change(function () {
          minLimitsGUI(thrustMinGUI, thrustMaxGUI);
      });
      //max thrust
      thrustMaxGUI.attr({
          step: convertScale('thrust', SCALES.thrust),
          min: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMin).toFixed(1),
          max: UNITS.convertToDisplay('thrust', MERGED_LIMITS_15xx.thrustMax).toFixed(0)
      });
      thrustMaxGUI.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMax).toFixed(1));
      thrustMaxGUI.change(function () {
          maxLimitsGUI(thrustMinGUI, thrustMaxGUI);
      });
      //system thrust
      $('.tab-cutoffs #systemThrust').html(UNITS.convertToDisplay('thrust', MERGED_LIMITS_15xx.thrustMin).toFixed(0) + ' / ' + UNITS.convertToDisplay('thrust', MERGED_LIMITS_15xx.thrustMax).toFixed(0));
      //min torque
      torqueMinGUI.attr({
          step: convertScale('torque', SCALES.torque),
          min: UNITS.convertToDisplay('torque', MERGED_LIMITS_15xx.torqueMin).toFixed(torqueDecimals),
          max: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMax).toFixed(torqueDecimals)
      });
      torqueMinGUI.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMin).toFixed(torqueDecimals));
      torqueMinGUI.change(function () {
          minLimitsGUI(torqueMinGUI, torqueMaxGUI);
      });
      //max torque
      torqueMaxGUI.attr({
          step: convertScale('torque', SCALES.torque),
          min: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMin).toFixed(torqueDecimals),
          max: UNITS.convertToDisplay('torque', MERGED_LIMITS_15xx.torqueMax).toFixed(torqueDecimals)
      });
      torqueMaxGUI.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMax).toFixed(torqueDecimals));
      torqueMaxGUI.change(function () {
          maxLimitsGUI(torqueMinGUI, torqueMaxGUI);
      });
      //system torque
      $('.tab-cutoffs #systemTorque').html(UNITS.convertToDisplay('torque', MERGED_LIMITS_15xx.torqueMin).toFixed(torqueDecimals) + ' / ' + UNITS.convertToDisplay('torque', MERGED_LIMITS_15xx.torqueMax).toFixed(torqueDecimals));

      //min motorSpeed
      rpmMinGUI.attr({
          step: SCALES.motorSpeed,
          min: UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_15xx.rpmMin).toFixed(0),
          max: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMax).toFixed(0)
      });
      rpmMinGUI.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMin).toFixed(0));
      rpmMinGUI.change(function () {
          minLimitsGUI(rpmMinGUI, rpmMaxGUI);
      });
      //max motorSpeed
      rpmMaxGUI.attr({
          step: SCALES.motorSpeed,
          min: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMin).toFixed(0),
          max: UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_15xx.rpmMax).toFixed(0)
      });
      rpmMaxGUI.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMax).toFixed(0));
      rpmMaxGUI.change(function () {
          maxLimitsGUI(rpmMinGUI, rpmMaxGUI);
      });
      //system RPM
      $('.tab-cutoffs #systemRpm').html(UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_15xx.rpmMin).toFixed(0) + ' / ' + UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_15xx.rpmMax).toFixed(0) + ' <img src="images/question.png" alt="Pole Info" title="This value depends on the number of motor poles (or on the number of divisors if you use the optical rpm probe)." height="15" width="15">');

      //min vibration
      vibrationMinGUI.attr({
          step: SCALES.vibration,
          min: MERGED_LIMITS_15xx.vibrationMin,
          max: USER_LIMITS.vibrationMax
      });
      vibrationMinGUI.val(USER_LIMITS.vibrationMin);
      vibrationMinGUI.change(function () {
          minLimitsGUI(vibrationMinGUI, vibrationMaxGUI);
      });
      //max vibration
      vibrationMaxGUI.attr({
          step: SCALES.vibration,
          min: USER_LIMITS.vibrationMin,
          max: MERGED_LIMITS_15xx.vibrationMax
      });
      vibrationMaxGUI.val(USER_LIMITS.vibrationMax);
      vibrationMaxGUI.change(function () {
          maxLimitsGUI(vibrationMinGUI, vibrationMaxGUI);
      });
      //system vibration
      $('.tab-cutoffs #systemVibration').html(MERGED_LIMITS_15xx.vibrationMin + ' / ' + MERGED_LIMITS_15xx.vibrationMax);
      
      var us = "";
      var protocol = TABS.motors.getControlProtocol();
      if(CONTROL_PROTOCOLS[protocol].microseconds){
        $(".microseconds").show();
      }else{
        $(".microseconds").hide();
      }
    }  
  
    //temperatures
    for(var i=0; i<temperatures.length; i++){   
        var temp = temperatures[i];
        var id = temp.id;

        function generateCallback(fct, i){
            var min = tempMinGUI[i];
            var max = tempMaxGUI[i];
            var result = function(){
                fct(min, max);
            }
            return result;
        }

        //min temp
        tempMinGUI[i].attr({
            step: SCALES.temperature,
            min: UNITS.convertToDisplay('temperature', MERGED_LIMITS_15xx.temperatureMin),
            max: UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMax[id])
        });
        tempMinGUI[i].val(UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMin[id]));
        tempMinGUI[i].change(generateCallback(minLimitsGUI, i));
        //max temp
        tempMaxGUI[i].attr({
            step: SCALES.temperature,
            min: UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMin[id]),
            max: UNITS.convertToDisplay('temperature', MERGED_LIMITS_15xx.temperatureMax)
        });
        tempMaxGUI[i].val(UNITS.convertToDisplay('temperature', USER_LIMITS.temperaturesMax[id]));
        tempMaxGUI[i].change(generateCallback(maxLimitsGUI, i)); 
    }
    //system temp
    $('.tab-cutoffs #systemTemp').html(UNITS.convertToDisplay('temperature', MERGED_LIMITS_15xx.temperatureMin) + ' / ' + UNITS.convertToDisplay('temperature', MERGED_LIMITS_15xx.temperatureMax)); 

    //Reveal new rows in GUI for temperature probes
    for(var i=0; i<temperatures.length; i++){
        $('#cutoffRowTemp'+(i+1)).show();
    }
  
    if(CONFIG.boardVersion === "Series 1780"){
      //min voltage
      voltageMinGUI.attr({
          step: SCALES.voltage,
          min: MERGED_LIMITS_1780.voltageMinA,
          max: USER_LIMITS.voltageMaxA
      });
      voltageMinGUI.val(USER_LIMITS.voltageMinA);
      voltageMinGUI.change(function () {
          minLimitsGUI(voltageMinGUI, voltageMaxGUI);
      });
      //max voltage
      voltageMaxGUI.attr({
          step: SCALES.voltage,
          min: USER_LIMITS.voltageMinA,
          max: MERGED_LIMITS_1780.voltageMaxA
      });
      voltageMaxGUI.val(USER_LIMITS.voltageMaxA);
      voltageMaxGUI.change(function () {
          maxLimitsGUI(voltageMinGUI, voltageMaxGUI);
      });
      //system voltage
      $('.tab-cutoffs #systemVoltage').html(MERGED_LIMITS_1780.voltageMin + ' / ' + MERGED_LIMITS_1780.voltageMaxA);
      $('.tab-cutoffs #systemVoltageB').html(MERGED_LIMITS_1780.voltageMin + ' / ' + MERGED_LIMITS_1780.voltageMaxB);

      //min current
      currentMinGUI.attr({
          step: SCALES.current,
          min: MERGED_LIMITS_1780.currentMinA,
          max: USER_LIMITS.currentMax
      });
      currentMinGUI.val(USER_LIMITS.currentMin);
      currentMinGUI.change(function () {
          minLimitsGUI(currentMinGUI, currentMaxGUI);
      });
      //max current
      currentMaxGUI.attr({
          step: SCALES.current,
          min: USER_LIMITS.currentMin,
          max: MERGED_LIMITS_1780.currentMaxA
      });
      currentMaxGUI.val(USER_LIMITS.currentMax);
      currentCableWarning();
      currentMaxGUI.change(function () {
          maxLimitsGUI(currentMinGUI, currentMaxGUI);
          currentCableWarning();
      });
      //system current
      $('.tab-cutoffs #systemCurrent').html(MERGED_LIMITS_1780.currentMinA + ' / ' + MERGED_LIMITS_1780.currentMaxA);
      $('.tab-cutoffs #systemCurrentB').html(MERGED_LIMITS_1780.currentMinB + ' / ' + MERGED_LIMITS_1780.currentMaxB);

      //min currentBurst
      currentBurstMinGUI.attr({
          step: SCALES.currentBurst,
          min: MERGED_LIMITS_1780.currentBurstMinA,
          max: USER_LIMITS.currentBurstMax
      });
      currentBurstMinGUI.val(USER_LIMITS.currentBurstMin);
      currentBurstMinGUI.change(function () {
          minLimitsGUI(currentBurstMinGUI, currentBurstMaxGUI);
      });
      //max currentBurst
      currentBurstMaxGUI.attr({
          step: SCALES.currentBurst,
          min: USER_LIMITS.currentBurstMin,
          max: MERGED_LIMITS_1780.currentBurstMaxA
      });
      currentBurstMaxGUI.val(USER_LIMITS.currentBurstMax);
      currentBurstMaxGUI.change(function () {
          maxLimitsGUI(currentBurstMinGUI, currentBurstMaxGUI);
      });
      //system currentBurst
      $('.tab-cutoffs #systemCurrentBurst').html(MERGED_LIMITS_1780.currentBurstMinA + ' / ' + MERGED_LIMITS_1780.currentBurstMaxA);
      $('.tab-cutoffs #systemCurrentBurstB').html(MERGED_LIMITS_1780.currentBurstMinB + ' / ' + MERGED_LIMITS_1780.currentBurstMaxB);

      //min power
      powerMinGUI.attr({
          step: SCALES.power,
          min: MERGED_LIMITS_1780.powerMinA,
          max: USER_LIMITS.powerMaxA
      });
      powerMinGUI.val(USER_LIMITS.powerMinA);
      powerMinGUI.change(function () {
          minLimitsGUI(powerMinGUI, powerMaxGUI);
      });
      //max power
      powerMaxGUI.attr({
          step: SCALES.power,
          min: USER_LIMITS.powerMinA,
          max: MERGED_LIMITS_1780.powerMaxA
      });
      powerMaxGUI.val(USER_LIMITS.powerMaxA);
      powerMaxGUI.change(function () {
          maxLimitsGUI(powerMinGUI, powerMaxGUI);
      });
      //system power
      $('.tab-cutoffs #systemPower').html(MERGED_LIMITS_1780.powerMinA + ' / ' + MERGED_LIMITS_1780.powerMaxA);
      $('.tab-cutoffs #systemPowerB').html(MERGED_LIMITS_1780.powerMinB + ' / ' + MERGED_LIMITS_1780.powerMaxB);

      //min thrust
      thrustMinGUI.attr({
          step: convertScale('thrust', SCALES.thrust),
          min: UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMinA).toFixed(0),
          max: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMax).toFixed(1)
      });
      thrustMinGUI.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMin).toFixed(1));
      thrustMinGUI.change(function () {
          minLimitsGUI(thrustMinGUI, thrustMaxGUI);
      });
      //max thrust
      thrustMaxGUI.attr({
          step: convertScale('thrust', SCALES.thrust),
          min: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMin).toFixed(1),
          max: UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMaxA).toFixed(0)
      });
      thrustMaxGUI.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMax).toFixed(1));
      thrustMaxGUI.change(function () {
          maxLimitsGUI(thrustMinGUI, thrustMaxGUI);
      });
      //system thrust
      $('.tab-cutoffs #systemThrust').html(UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMinA).toFixed(0) + ' / ' + UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMaxA).toFixed(0));
      $('.tab-cutoffs #systemThrustB').html(UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMinB).toFixed(0) + ' / ' + UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMaxB).toFixed(0));
      //min torque
      torqueMinGUI.attr({
          step: convertScale('torque', SCALES.torque),
          min: UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMinA).toFixed(torqueDecimals),
          max: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMax).toFixed(torqueDecimals)
      });
      torqueMinGUI.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMin).toFixed(torqueDecimals));
      torqueMinGUI.change(function () {
          minLimitsGUI(torqueMinGUI, torqueMaxGUI);
      });
      //max torque
      torqueMaxGUI.attr({
          step: convertScale('torque', SCALES.torque),
          min: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMin).toFixed(torqueDecimals),
          max: UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMaxA).toFixed(torqueDecimals)
      });
      torqueMaxGUI.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMax).toFixed(torqueDecimals));
      torqueMaxGUI.change(function () {
          maxLimitsGUI(torqueMinGUI, torqueMaxGUI);
      });
      //system torque
      $('.tab-cutoffs #systemTorque').html(UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMinA).toFixed(torqueDecimals) + ' / ' + UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMaxA).toFixed(torqueDecimals));
      $('.tab-cutoffs #systemTorqueB').html(UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMinB).toFixed(torqueDecimals) + ' / ' + UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMaxB).toFixed(torqueDecimals));

      //min motorSpeed
      rpmMinGUI.attr({
          step: SCALES.motorSpeed,
          min: UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMin).toFixed(0),
          max: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMax).toFixed(0)
      });
      rpmMinGUI.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMin).toFixed(0));
      rpmMinGUI.change(function () {
          minLimitsGUI(rpmMinGUI, rpmMaxGUI);
      });
      //max motorSpeed
      rpmMaxGUI.attr({
          step: SCALES.motorSpeed,
          min: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMin).toFixed(0),
          max: UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMax).toFixed(0)
      });
      rpmMaxGUI.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMax).toFixed(0));
      rpmMaxGUI.change(function () {
          maxLimitsGUI(rpmMinGUI, rpmMaxGUI);
      });
      //system RPM
      $('.tab-cutoffs #systemRpm').html(UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMin).toFixed(0) + ' / ' + UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMax).toFixed(0) + ' <img src="images/question.png" alt="Pole Info" title="This value depends on the number of motor poles (or on the number of divisors if you use the optical rpm probe)." height="15" width="15">');
      $('.tab-cutoffs #systemRpmB').html(UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMin).toFixed(0) + ' / ' + UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMaxB).toFixed(0) + ' <img src="images/question.png" alt="Pole Info" title="This value depends on the number of motor poles (or on the number of divisors if you use the optical rpm probe)." height="15" width="15">');
      
      //Cutoff switch
      var checkboxCutoff = $("#reverse_switch");
      checkboxCutoff.prop('checked', CONFIG.reverseCutoffSwitch.value);
      checkboxCutoff.click(function(){
        changeMemory(CONFIG.reverseCutoffSwitch, checkboxCutoff.is(':checked'));
      });
      
      
      //***** SIDE B ******
      //min voltage
      voltageMinGUIB.attr({
          step: SCALES.voltage,
          min: MERGED_LIMITS_1780.voltageMinB,
          max: USER_LIMITS.voltageMaxB
      });
      voltageMinGUIB.val(USER_LIMITS.voltageMinB);
      voltageMinGUIB.change(function () {
          minLimitsGUI(voltageMinGUIB, voltageMaxGUIB);
      });
      //max voltage
      voltageMaxGUIB.attr({
          step: SCALES.voltage,
          min: USER_LIMITS.voltageMinB,
          max: MERGED_LIMITS_1780.voltageMaxB
      });
      voltageMaxGUIB.val(USER_LIMITS.voltageMaxB);
      voltageMaxGUIB.change(function () {
          maxLimitsGUI(voltageMinGUIB, voltageMaxGUIB);
      });

      //min current
      currentMinGUIB.attr({
          step: SCALES.current,
          min: MERGED_LIMITS_1780.currentMinB,
          max: USER_LIMITS.currentMaxB
      });
      currentMinGUIB.val(USER_LIMITS.currentMinB);
      currentMinGUIB.change(function () {
          minLimitsGUI(currentMinGUIB, currentMaxGUIB);
      });
      //max current
      currentMaxGUIB.attr({
          step: SCALES.current,
          min: USER_LIMITS.currentMinB,
          max: MERGED_LIMITS_1780.currentMaxB
      });
      currentMaxGUIB.val(USER_LIMITS.currentMaxB);
      currentMaxGUIB.change(function () {
          maxLimitsGUI(currentMinGUIB, currentMaxGUIB);
      });
      
      //min currentBurst
      currentBurstMinGUIB.attr({
          step: SCALES.currentBurst,
          min: MERGED_LIMITS_1780.currentBurstMinB,
          max: USER_LIMITS.currentBurstMaxB
      });
      currentBurstMinGUIB.val(USER_LIMITS.currentBurstMinB);
      currentBurstMinGUIB.change(function () {
          minLimitsGUI(currentBurstMinGUIB, currentBurstMaxGUIB);
      });
      //max currentBurst
      currentBurstMaxGUIB.attr({
          step: SCALES.currentBurst,
          min: USER_LIMITS.currentBurstMinB,
          max: MERGED_LIMITS_1780.currentBurstMaxB
      });
      currentBurstMaxGUIB.val(USER_LIMITS.currentBurstMaxB);
      currentBurstMaxGUIB.change(function () {
          maxLimitsGUI(currentBurstMinGUIB, currentBurstMaxGUIB);
      });

      //min power
      powerMinGUIB.attr({
          step: SCALES.power,
          min: MERGED_LIMITS_1780.powerMinB,
          max: USER_LIMITS.powerMaxB
      });
      powerMinGUIB.val(USER_LIMITS.powerMinB);
      powerMinGUIB.change(function () {
          minLimitsGUI(powerMinGUIB, powerMaxGUIB);
      });
      //max power
      powerMaxGUIB.attr({
          step: SCALES.power,
          min: USER_LIMITS.powerMinB,
          max: MERGED_LIMITS_1780.powerMaxB
      });
      powerMaxGUIB.val(USER_LIMITS.powerMaxB);
      powerMaxGUIB.change(function () {
          maxLimitsGUI(powerMinGUIB, powerMaxGUIB);
      });

      //min thrust
      thrustMinGUIB.attr({
          step: convertScale('thrust', SCALES.thrust),
          min: UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMinB).toFixed(0),
          max: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMaxB).toFixed(1)
      });
      thrustMinGUIB.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMinB).toFixed(1));
      thrustMinGUIB.change(function () {
          minLimitsGUI(thrustMinGUIB, thrustMaxGUIB);
      });
      //max thrust
      thrustMaxGUIB.attr({
          step: convertScale('thrust', SCALES.thrust),
          min: UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMinB).toFixed(1),
          max: UNITS.convertToDisplay('thrust', MERGED_LIMITS_1780.thrustMaxB).toFixed(0)
      });
      thrustMaxGUIB.val(UNITS.convertToDisplay('thrust', USER_LIMITS.thrustMaxB).toFixed(1));
      thrustMaxGUIB.change(function () {
          maxLimitsGUI(thrustMinGUIB, thrustMaxGUIB);
      });
      
      //min torque
      torqueMinGUIB.attr({
          step: convertScale('torque', SCALES.torque),
          min: UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMinB).toFixed(torqueDecimals),
          max: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMaxB).toFixed(torqueDecimals)
      });
      torqueMinGUIB.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMinB).toFixed(torqueDecimals));
      torqueMinGUIB.change(function () {
          minLimitsGUI(torqueMinGUIB, torqueMaxGUIB);
      });
      //max torque
      torqueMaxGUIB.attr({
          step: convertScale('torque', SCALES.torque),
          min: UNITS.convertToDisplay('torque', USER_LIMITS.torqueMinB).toFixed(torqueDecimals),
          max: UNITS.convertToDisplay('torque', MERGED_LIMITS_1780.torqueMaxB).toFixed(torqueDecimals)
      });
      torqueMaxGUIB.val(UNITS.convertToDisplay('torque', USER_LIMITS.torqueMaxB).toFixed(torqueDecimals));
      torqueMaxGUIB.change(function () {
          maxLimitsGUI(torqueMinGUIB, torqueMaxGUIB);
      });

      //min motorSpeed
      rpmMinGUIB.attr({
          step: SCALES.motorSpeed,
          min: UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMinB).toFixed(0),
          max: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMaxB).toFixed(0)
      });
      rpmMinGUIB.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMinB).toFixed(0));
      rpmMinGUIB.change(function () {
          minLimitsGUI(rpmMinGUIB, rpmMaxGUIB);
      });
      //max motorSpeed
      rpmMaxGUIB.attr({
          step: SCALES.motorSpeed,
          min: UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMinB).toFixed(0),
          max: UNITS.convertToDisplay('motorSpeed', MERGED_LIMITS_1780.rpmMaxB).toFixed(0)
      });
      rpmMaxGUIB.val(UNITS.convertToDisplay('motorSpeed', USER_LIMITS.rpmMaxB).toFixed(0));
      rpmMaxGUIB.change(function () {
          maxLimitsGUI(rpmMinGUIB, rpmMaxGUIB);
      });
    }
	
	//save user cutoff values
	$('.tab-cutoffs input').change(function () {
        if(CONFIG.boardVersion === "Series 1780"){
          USER_LIMITS.voltageMinA = Number(voltageMinGUI.val());
          USER_LIMITS.voltageMaxA = Number(voltageMaxGUI.val());
          USER_LIMITS.currentMin = Number(currentMinGUI.val());
          USER_LIMITS.currentMax = Number(currentMaxGUI.val());
          USER_LIMITS.currentBurstMin = Number(currentBurstMinGUI.val());
          USER_LIMITS.currentBurstMax = Number(currentBurstMaxGUI.val());
          USER_LIMITS.powerMinA = Number(powerMinGUI.val());
          USER_LIMITS.powerMaxA = Number(powerMaxGUI.val());
          USER_LIMITS.thrustMin = Number(UNITS.convertToWorking('thrust', thrustMinGUI.val()).toFixed(2));
          USER_LIMITS.thrustMax = Number(UNITS.convertToWorking('thrust', thrustMaxGUI.val()).toFixed(2));
          USER_LIMITS.torqueMin = Number(UNITS.convertToWorking('torque', torqueMinGUI.val()).toFixed(torqueDecimals));
          USER_LIMITS.torqueMax = Number(UNITS.convertToWorking('torque', torqueMaxGUI.val()).toFixed(torqueDecimals));
          USER_LIMITS.rpmMin = Number(UNITS.convertToWorking('motorSpeed', rpmMinGUI.val()).toFixed(0));
          USER_LIMITS.rpmMax = Number(UNITS.convertToWorking('motorSpeed', rpmMaxGUI.val()).toFixed(0));
        }else{
          USER_LIMITS.voltageMin = Number(voltageMinGUI.val());
          USER_LIMITS.voltageMax = Number(voltageMaxGUI.val());
          USER_LIMITS.currentMin = Number(currentMinGUI.val());
          USER_LIMITS.currentMax = Number(currentMaxGUI.val());
          USER_LIMITS.currentBurstMin = Number(currentBurstMinGUI.val());
          USER_LIMITS.currentBurstMax = Number(currentBurstMaxGUI.val());
          USER_LIMITS.powerMin = Number(powerMinGUI.val());
          USER_LIMITS.powerMax = Number(powerMaxGUI.val());
          USER_LIMITS.thrustMin = Number(UNITS.convertToWorking('thrust', thrustMinGUI.val()).toFixed(2));
          USER_LIMITS.thrustMax = Number(UNITS.convertToWorking('thrust', thrustMaxGUI.val()).toFixed(2));
          USER_LIMITS.torqueMin = Number(UNITS.convertToWorking('torque', torqueMinGUI.val()).toFixed(torqueDecimals));
          USER_LIMITS.torqueMax = Number(UNITS.convertToWorking('torque', torqueMaxGUI.val()).toFixed(torqueDecimals));
          USER_LIMITS.rpmMin = Number(UNITS.convertToWorking('motorSpeed', rpmMinGUI.val()).toFixed(0));
          USER_LIMITS.rpmMax = Number(UNITS.convertToWorking('motorSpeed', rpmMaxGUI.val()).toFixed(0));
        }
		
      
        USER_LIMITS.voltageMinB = Number(voltageMinGUIB.val());
		USER_LIMITS.voltageMaxB = Number(voltageMaxGUIB.val());
		USER_LIMITS.currentMinB = Number(currentMinGUIB.val());
		USER_LIMITS.currentMaxB = Number(currentMaxGUIB.val());
        USER_LIMITS.currentBurstMinB = Number(currentBurstMinGUIB.val());
		USER_LIMITS.currentBurstMaxB = Number(currentBurstMaxGUIB.val());
        USER_LIMITS.powerMinB = Number(powerMinGUIB.val());
		USER_LIMITS.powerMaxB = Number(powerMaxGUIB.val());
		USER_LIMITS.thrustMinB = Number(UNITS.convertToWorking('thrust', thrustMinGUIB.val()).toFixed(2));
		USER_LIMITS.thrustMaxB = Number(UNITS.convertToWorking('thrust', thrustMaxGUIB.val()).toFixed(2));
		USER_LIMITS.torqueMinB = Number(UNITS.convertToWorking('torque', torqueMinGUIB.val()).toFixed(torqueDecimals));
		USER_LIMITS.torqueMaxB = Number(UNITS.convertToWorking('torque', torqueMaxGUIB.val()).toFixed(torqueDecimals));
		USER_LIMITS.rpmMinB = Number(UNITS.convertToWorking('motorSpeed', rpmMinGUIB.val()).toFixed(0));
		USER_LIMITS.rpmMaxB = Number(UNITS.convertToWorking('motorSpeed', rpmMaxGUIB.val()).toFixed(0));
      
        USER_LIMITS.vibrationMin = Number(vibrationMinGUI.val());
		USER_LIMITS.vibrationMax = Number(vibrationMaxGUI.val());
        //temperatures
        for(var i=0; i<temperatures.length; i++){   
            var temp = temperatures[i];
            var id = temp.id;
            USER_LIMITS.temperaturesMin[id] = Number(UNITS.convertToWorking('temperature',tempMinGUI[i].val()));
            USER_LIMITS.temperaturesMax[id] = Number(UNITS.convertToWorking('temperature',tempMaxGUI[i].val()));
        }
		chrome.storage.local.set({'USER_LIMITS': USER_LIMITS});
	});
  
    showBoardSpecific(false);
}

TABS.cutoffs = {};
TABS.cutoffs.initialize = function (callback) {
    var self = this;

    if (GUI.active_tab != 'cutoffs') {
        GUI.active_tab = 'cutoffs';
        googleAnalytics.sendAppView('Cutoffs');
    }

    $('#tabContent').load("./tabs/cutoffs.html", function () {

        // translate to user-selected language
        //localize();
		
        
        showBoardSpecific(false);
		
		refreshCutoffs();
        
        $("img#burstCurrentPopup").click(function(){				   
			$('#burstCurrentDialog').load("../burstcurrent.html", function (content) {
				translateAll();
				$('#burstCurrentDialog').dialog({
					show: {
						effect: "blind",
						duration: 500
					},
					hide: {
						effect: "blind",
						duration: 500
					},
					title: getMessage("currentBurstInformation"),
					resizable: true,
					draggable: true,
					height: 600,
					width: 600,
					dialogClass: 'ui-dialog-osx',
					modal: true,
					open : function() {
					   $(this).parent().promise().done(function () {
					   	$(".ui-dialog-content").scrollTop(0);
					   	//console.log("scrolling to top");
					   });
					},
                    close: function(event, ui){
                        $(this).dialog('destroy');
                    }
				});
			});	
			googleAnalytics.sendEvent('BurstCurrentHelp', 'Click');	
		});
        
        // Setup "?" tooltips
        $("#limits > tbody > tr:nth-child(9) > td:nth-child(1) > img").tooltip({ show: { effect: "blind", duration: 100 } });
		$("#systemRpm > img").tooltip({ show: { effect: "blind", duration: 100 } });
        $("#cablesWarning").tooltip({ show: { effect: "blind", duration: 100 } });
		translateAll();
        if (callback) callback();
		
    });
	
};

TABS.cutoffs.cleanup = function (callback) {
    if (callback) callback();
};

function minLimitsGUI(min, max) {
	var minVal = Number(min.val());
	var minMin = Number(min.attr('min'));
	var minMax = Number(min.attr('max'));
	if(minVal < minMin){
		max.attr('min', min.attr('min'));
		min.val(min.attr('min'));
	}
	else if(minVal > minMax){
		max.attr('min', min.attr('max'));
		min.val(min.attr('max'));
	}
	else
		max.attr('min', min.val());
    setTimeout(currentCableWarning,300);
}

function maxLimitsGUI(min, max) {
	var maxVal = Number(max.val());
	var maxMin = Number(max.attr('min'));
	var maxMax = Number(max.attr('max'));
	if(maxVal < maxMin){
		min.attr('max', max.attr('min'));
		max.val(max.attr('min'));
	}
	else if(maxVal > maxMax){
		min.attr('max', max.attr('max'));
		max.val(max.attr('max'));
	}
	else
		min.attr('max', max.val());
    setTimeout(currentCableWarning,300);
}

function convertScale(sensor, scale) {
	var convertedScale = UNITS.convertToDisplay(sensor, scale);
	var order = Math.floor(Math.log(convertedScale) / Math.LN10 + 0.000000001);
	var step = Math.pow(10, order);
    return step;
}