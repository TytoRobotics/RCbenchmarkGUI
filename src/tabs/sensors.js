'use strict';

var PLOTSinfo = {};

TABS.sensors = {};

TABS.sensors.initialize = function (callback) {
	
	
	$('#plot').scroll(function () {
		$('.tab-sensors .info').css('top', $('#plot').scrollTop());
	});
	
    var self = this, names = [], labels = [], classes = [];
    

    var checkboxDefs = {
        checkboxesNames:  names,
        checkboxesLabels: labels,
        checkboxesClasses: classes
    };


    //List of plots and their properties
    var plotDefs = {};
    
    var temperatures = DATA.getTemperatures();
    
    function addCheckBox(name, label, htmlClass){
        names.push(name);
        labels.push(label);
        classes.push(htmlClass);
    }
    

    if(CONFIG.boardVersion === "Series 1580"){
        
        addCheckBox("accel", "Acc"); 
        addCheckBox("torque", "Torque"); 
        addCheckBox("loadcells", "Load Cells");
    }
  
    if(temperatures.length>0){
        addCheckBox("temps", "temp");
    }
  
    if(CONFIG.boardVersion === "Series 1580" || CONFIG.boardVersion === "Series 1780"){
      if(DATA.isPressureSensorAvailable()){
            addCheckBox("airspeed", getMessage('airSpeed') + " 	<img src='images/question.png' title='" + getMessage('airSpeedTooltip') + "' height='15' width='15'>", true);

            plotDefs["airspeed"] = {
                title:          getMessage('airSpeed')+" - " + UNITS.text[UNITS.display['speed']],
                checkbox:       "airspeed",
                displayDigits:  2,
                plots: [
                    {label:  getMessage('airSpeed'), dataFct:  function() {return UNITS.convertToDisplay('speed',DATA.getAirSpeed())}},
                ]
            };

            plotDefs["eFlightEfficiency"] = {
                title:          getMessage('electricalForwardFlightEfficiency')+" - %",
                checkbox:       "airspeed",
                displayDigits:  2,
                plots: [
                    {label: getMessage('flightEfficiency'), dataFct:  function() {
                            var val = DATA.getForwardFlightEfficiency(); 
                            return Math.min(Math.max(-150, val), 150);
                        }
                    },
                ]
            };
        } 
    }
        
    if(temperatures.length>0){
        plotDefs["temps"]= {
            title:          getMessage('temp')+" - " + UNITS.text[UNITS.display['temperature']],
            checkbox:       "temps",
            displayDigits:  1,
            fixedDigits: true,  // Display fix number of digit after the "."
            plots: []
        };
        for(var i=0; i<temperatures.length; i++){
            var temp = temperatures[i];
            var makeFunction = (function(index){
                return function(){
                    var temps = DATA.getTemperatures();
                    var temp = temps[index];
                    var value = 0;
                    if(temp===undefined && callback!=="noloop"){
                        TABS.sensors.initialize("noloop");
                    }else{
                        value = UNITS.convertToDisplay('temperature',temp.value);
                    }
                    return value;                    
                }
            }(i));
            plotDefs.temps.plots.push({
                label:  temp.label, 
                dataFct:  makeFunction
            });
        }            
    }
  
    if(CONFIG.boardVersion === "Series 1580"){
        plotDefs["accel"] = {
            title:          getMessage('Accelerometer')+" - g",
            checkbox:       "accel",
            displayDigits:  3,
            fixedDigits: true,  // Display fix number of digit after the "."
            plots: [
                {label:  'X', dataFct:  function() {return DATA.getAccelerometer(0)}},
                {label:  'Y', dataFct:  function() {return DATA.getAccelerometer(1)}},
                {label:  'Z', dataFct:  function() {return DATA.getAccelerometer(2)}}
            ]
        };
        
        plotDefs["accelvib"] = {
            title:          getMessage('vibrationForSensor')+" - g",
            checkbox:       "accel",
            displayDigits:  3,
            fixedDigits: true,  // Display fix number of digit after the "."
            plots: [
                {label:  getMessage('vibrationForSensor'), dataFct:  function() {return SENSOR_DATA.vibration}}
            ]
        };
        
        plotDefs["torque"] = {
            title:          getMessage('Torque')+" - " + UNITS.text[UNITS.display['torque']],
            checkbox:       "torque",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return UNITS.convertToDisplay('torque', DATA.getTorque())}}
            ]
        };
        
        plotDefs["loadcells"] = {
            title:          getMessage('Load Cells')+" - mV",
            checkbox:       "loadcells",
            displayDigits:  3,
            plots: [
                {label:  getMessage('Thrust'), dataFct:  function() {return DATA.getLoadCellThrust()}},
                {label:  getMessage('left'), dataFct:  function() {return DATA.getLoadCellLeft()}},
                {label:  getMessage('right'), dataFct:  function() {return DATA.getLoadCellRight()}}
            ]
        };
        
        plotDefs["electricalpower"] = {
            title:          getMessage('power')+" -" + UNITS.text[UNITS.display['power']],
            checkbox:       "escpower",
            displayDigits:  2,
            plots: [
                {label:  getMessage('elecPower'), dataFct:  function() {return DATA.getElectricalPower()}},
                {label:  getMessage('mechPower'), dataFct:  function() {return DATA.getMechanicalPower()}},
            ]
        };
        
        plotDefs["efficiencyMotor"] = {
            title:          getMessage('motorEfficiency')+" - %",
            checkbox:       "efficiency",
            displayDigits:  2,
            plots: [
                {label:  getMessage('motorEfficiency'), dataFct:  function() {return DATA.getMotorEfficiency()}},
            ],
            range: [0, 100]
        };
        
        plotDefs["efficiencyPropeller"] = {
            title:          getMessage('propellerEfficiency')+" - " + UNITS.text[UNITS.display['thrust']] + "/W",
            checkbox:       "efficiency",
            displayDigits:  3,
            plots: [
                {label: getMessage('mechEfficiency'), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropMechEfficiency())}},
                {label:  getMessage('overAllEfficiency'), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropElecEfficiency())}},
            ],
            range: [0, UNITS.convertToDisplay('thrust', 0.05)] // 50g/W max is realistic
        };
        
    }
    
    
    if(CONFIG.boardVersion === "Series 1520"){
        
        addCheckBox("loadcells", "Load Cell");
        
        plotDefs["loadcells"] = {
            title:          getMessage('Load Cell')+" - mV",
            checkbox:       "loadcells",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return DATA.getLoadCellThrust()}}
            ]
        };
        
        plotDefs["electricalpower"] = {
            title:          getMessage('power')+ " -" + UNITS.text[UNITS.display['power']],
            checkbox:       "escpower",
            displayDigits:  2,
            plots: [
                {label: getMessage('elecPower'), dataFct:  function() {return DATA.getElectricalPower()}}
            ]
        };
        
        plotDefs["efficiencyPropeller"] = {
            title:          getMessage('overAllEfficiency')+" - " + UNITS.text[UNITS.display['thrust']] + "/W",
            checkbox:       "efficiency",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropElecEfficiency())}},
            ]
        };
    }
    
    if(CONFIG.boardVersion === "Series 1520" || CONFIG.boardVersion === "Series 1580"){
      plotDefs["thrust"] = {
          title: getMessage('Thrust')+ " - " + UNITS.text[UNITS.display['thrust']],	  	  
          checkbox:       "thrust",
          displayDigits:  3,
          plots: [
              {label:  '', dataFct:  function() {return UNITS.convertToDisplay('thrust', DATA.getThrust())}}
          ]
          //range: [UNITS.convertToDisplay('thrust', -10), UNITS.convertToDisplay('thrust', 10)]
      };    

      plotDefs["escvoltage"] = {
          title:          getMessage('ESC voltage')+" - V",
          checkbox:       "escpower",
          displayDigits:  3,
          plots: [
              {label:  '', dataFct:  function() {return DATA.getESCVoltage()}}
          ],
          range: [0, 50]
      };

      plotDefs["esccurrent"] = {
          title:          getMessage('ESC current')+" - A",
          checkbox:       "escpower",
          displayDigits:  3,
          plots: [
              {label:  '', dataFct:  function() {return DATA.getESCCurrent()}}
          ]
      };

      plotDefs["esccurrentburst"] = {
          title:          getMessage('ESC burst current')+" - A",
          checkbox:       "debug",
          displayDigits:  3,
          plots: [
              {label:  '', dataFct:  function() {return DATA.getESCCurrentBurst()}}
          ]
      };

      plotDefs["motorrpm"] = {
          title:          getMessage('motorRotationSpeed')+" - " + UNITS.text[UNITS.display['motorSpeed']],
          checkbox:       "motorrpm",
          displayDigits:  0,
          fixedDigits: true,  // Display fix number of digit after the "."
          plots: [
              {label:  getMessage('electrical'), dataFct:  function() {return UNITS.convertToDisplay('motorSpeed', DATA.getElectricalRPM())}},
              {label:  getMessage('optical'), dataFct:  function() {return UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM())}}
          ],
          range: [0, 200000]
      };

      addCheckBox("motorrpm", "RPM");
      addCheckBox("thrust", "Thrust");
      addCheckBox("escpower", "escPower");
      addCheckBox("efficiency", "Efficiency");
    }
  
    if(CONFIG.boardVersion === "Series 1780"){
      addCheckBox("torque", "Torque"); 
      addCheckBox("thrust", "Thrust");
      addCheckBox("loadcells", "Load Cells");
      addCheckBox("motorrpm", "RPM");
      addCheckBox("escpower", "escPower");
      addCheckBox("efficiency", "Efficiency");
      
      if(CONFIG.s1780detected.LCB){
        plotDefs["torque"] = {
            title:          getMessage("Torque")+ " - " + UNITS.text[UNITS.display['torque']],
            checkbox:       "torque",
            displayDigits:  3,
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return UNITS.convertToDisplay('torque', DATA.getTorque())}},
                {label:  getMessage("sideB"), dataFct:  function() {return UNITS.convertToDisplay('torque', DATA.getTorque(true))}},
                {label:  getMessage('total'), dataFct:  function() {return UNITS.convertToDisplay('torque', DATA.getDualTorque())}}
            ]
        }; 
        
        plotDefs["electricalpowerA"] = {
            title:          getMessage("sideA") + ": " + getMessage("power") + " " + UNITS.text[UNITS.display['power']],
            checkbox:       "escpower",
            displayDigits:  2,
            plots: [
                {label:  getMessage('elecPower'), dataFct:  function() {return DATA.getElectricalPower()}},
                {label:  getMessage('mechPower'), dataFct:  function() {return DATA.getMechanicalPower()}},
            ]
        };
        plotDefs["electricalpowerB"] = {
            title:          getMessage("sideB") + ": " + getMessage("power") + " " + UNITS.text[UNITS.display['power']],
            checkbox:       "escpower",
            displayDigits:  2,
            plots: [
                {label:  getMessage('elecPower'), dataFct:  function() {return DATA.getElectricalPower(true)}},
                {label:  getMessage('mechPower'), dataFct:  function() {return DATA.getMechanicalPower(true)}},
            ]
        };

        plotDefs["thrust"] = {
            title:          getMessage("Thrust")+" - " + UNITS.text[UNITS.display['thrust']],
            checkbox:       "thrust",
            displayDigits:  3,
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return UNITS.convertToDisplay('thrust', DATA.getThrust())}},
                {label:  getMessage("sideB"), dataFct:  function() {return UNITS.convertToDisplay('thrust', DATA.getThrust(true))}},
                {label:  getMessage('total'), dataFct:  function() {return UNITS.convertToDisplay('thrust', DATA.getDualThrust())}}
            ],
            range: [UNITS.convertToDisplay('thrust', -10), UNITS.convertToDisplay('thrust', 10)]
        };    

        plotDefs["escvoltage"] = {
            title:          getMessage("ESC voltage")+" - V",
            checkbox:       "escpower",
            displayDigits:  3,
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return DATA.getESCVoltage()}},
                {label:  getMessage("sideB"), dataFct:  function() {return DATA.getESCVoltage(true)}}
            ],
            range: [0, 50]
        };

        plotDefs["esccurrent"] = {
            title:          getMessage("ESC current")+" - A",
            checkbox:       "escpower",
            displayDigits:  3,
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return DATA.getESCCurrent()}},
                {label:  getMessage("sideB"), dataFct:  function() {return DATA.getESCCurrent(true)}}
            ]
        };

        plotDefs["esccurrentburst"] = {
            title:          getMessage("ESC burst current")+" - A",
            checkbox:       "debug",
            displayDigits:  3,
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return DATA.getESCCurrentBurst()}},
                {label:  getMessage("sideB"), dataFct:  function() {return DATA.getESCCurrentBurst(true)}}
            ]
        };

        plotDefs["motorrpm"] = {
            title:          getMessage("motorRotationSpeed")+" - " + UNITS.text[UNITS.display['motorSpeed']],
            checkbox:       "motorrpm",
            displayDigits:  0,
            fixedDigits: true,  // Display fix number of digit after the "."
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM())}},
                {label:  getMessage("sideB"), dataFct:  function() {return UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM(true))}}
            ],
            range: [0, 200000]
        };

        plotDefs["efficiencyMotor"] = {
            title:          getMessage("motorEfficiency")+" - %",
            checkbox:       "efficiency",
            displayDigits:  2,
            plots: [
                {label:  getMessage("sideA"), dataFct:  function() {return DATA.getMotorEfficiency()}},
                {label:  getMessage("sideB"), dataFct:  function() {return DATA.getMotorEfficiency(true)}},
            ],
            range: [0, 100]
        };

        plotDefs["efficiencyPropellerA"] = {
            title:          getMessage("propellerEff.sideA")+" - " + UNITS.text[UNITS.display['thrust']] + "/W",
            checkbox:       "efficiency",
            displayDigits:  3,
            plots: [
                {label:  getMessage("mechEfficiency"), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropMechEfficiency())}},
                {label:  getMessage("overAllEfficiency"), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropElecEfficiency())}},
            ],
            range: [0, UNITS.convertToDisplay('thrust', 0.05)] // 50g/W max is realistic
        };
        
        plotDefs["efficiencyPropellerB"] = {
            title:          getMessage("propellerEff.sideB")+" - " + UNITS.text[UNITS.display['thrust']] + "/W",
            checkbox:       "efficiency",
            displayDigits:  3,
            plots: [
                {label:  getMessage("mechEfficiency"), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropMechEfficiency(true))}},
                {label:  getMessage("overAllEfficiency"), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropElecEfficiency(true))}},
            ],
            range: [0, UNITS.convertToDisplay('thrust', 0.05)] // 50g/W max is realistic
        };
        
      }else{
        plotDefs["torque"] = {
            title:          getMessage("Torque")+" - " + UNITS.text[UNITS.display['torque']],
            checkbox:       "torque",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return UNITS.convertToDisplay('torque', DATA.getTorque())}}
            ]
        };
        
        plotDefs["electricalpower"] = {
            title:          getMessage("power")+" -" + UNITS.text[UNITS.display['power']],
            checkbox:       "escpower",
            displayDigits:  2,
            plots: [
                {label:  getMessage("elecPower"), dataFct:  function() {return DATA.getElectricalPower()}},
                {label:  getMessage("mechPower"), dataFct:  function() {return DATA.getMechanicalPower()}},
            ]
        };

        plotDefs["thrust"] = {
            title:          getMessage("Thrust")+" - " + UNITS.text[UNITS.display['thrust']],
            checkbox:       "thrust",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return UNITS.convertToDisplay('thrust', DATA.getThrust())}}
            ],
            range: [UNITS.convertToDisplay('thrust', -10), UNITS.convertToDisplay('thrust', 10)]
        };    

        plotDefs["escvoltage"] = {
            title:          getMessage("ESC voltage")+" - V",
            checkbox:       "escpower",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return DATA.getESCVoltage()}}
            ],
            range: [0, 50]
        };

        plotDefs["esccurrent"] = {
            title:          getMessage("ESC current")+" - A",
            checkbox:       "escpower",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return DATA.getESCCurrent()}}
            ]
        };

        plotDefs["esccurrentburst"] = {
            title:          getMessage("ESC burst current")+" - A",
            checkbox:       "debug",
            displayDigits:  3,
            plots: [
                {label:  '', dataFct:  function() {return DATA.getESCCurrentBurst()}}
            ]
        };

        plotDefs["motorrpm"] = {
            title:          getMessage("motorRotationSpeed")+" - " + UNITS.text[UNITS.display['motorSpeed']],
            checkbox:       "motorrpm",
            displayDigits:  0,
            fixedDigits: true,  // Display fix number of digit after the "."
            plots: [
                {label: getMessage("optical"), dataFct:  function() {return UNITS.convertToDisplay('motorSpeed', DATA.getOpticalRPM())}}
            ],
            range: [0, 200000]
        };

        plotDefs["efficiencyMotor"] = {
            title:          getMessage("motorEfficiency")+" - %",
            checkbox:       "efficiency",
            displayDigits:  2,
            plots: [
                {label:  getMessage("motorEfficiency"), dataFct:  function() {return DATA.getMotorEfficiency()}},
            ],
            range: [0, 100]
        };

        plotDefs["efficiencyPropeller"] = {
            title:          getMessage("propellerEfficiency")+" - " + UNITS.text[UNITS.display['thrust']] + "/W",
            checkbox:       "efficiency",
            displayDigits:  3,
            plots: [
                {label:  getMessage("motorEfficiency"), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropMechEfficiency())}},
                {label:  getMessage("overAllEfficiency"), dataFct:  function() {
                  return UNITS.convertToDisplay('thrust', DATA.getPropElecEfficiency())}},
            ],
            range: [0, UNITS.convertToDisplay('thrust', 0.05)] // 50g/W max is realistic
        };
      }

      if(CONFIG.s1780detected.LCA){
        var title = '';
        if(CONFIG.s1780detected.LCB){
          title = getMessage("sideA") + ': ';
        }
        var title1 = title + getMessage("thrustCells");
        plotDefs["loadcellsForcesA"] = {
            title:          title1,
            checkbox:       "loadcells",
            displayDigits:  3,
            plots: [
                {label:  'F1', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawA[0]}},
                {label:  'F2', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawA[2]}},
                {label:  'F3', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawA[4]}}
            ]
        };

        var title2 = title + getMessage("torqueCells"); 
        plotDefs["loadcellsTorquesA"] = {
            title:          title2,
            checkbox:       "loadcells",
            displayDigits:  3,
            plots: [
                {label:  'M1', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawA[1]}},
                {label:  'M2', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawA[3]}},
                {label:  'M3', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawA[5]}}
            ]
        }; 
      }
      
      if(CONFIG.s1780detected.LCB){
        plotDefs["loadcellsForcesB"] = {
            title:          getMessage("sideB") + ": "+ getMessage("thrustCells"),
            checkbox:       "loadcells",
            displayDigits:  3,
            plots: [
                {label:  'F1', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawB[0]}},
                {label:  'F2', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawB[2]}},
                {label:  'F3', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawB[4]}}
            ]
        };

        plotDefs["loadcellsTorquesB"] = {
            title:          getMessage("sideB") + ": "+ getMessage("torqueCells"),
            checkbox:       "loadcells",
            displayDigits:  3,
            plots: [
                {label:  'M1', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawB[1]}},
                {label:  'M2', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawB[3]}},
                {label:  'M3', dataFct:  function() {return SENSOR_DATA.sixAxisForcesRawB[5]}}
            ]
        }; 
      }
    }
  
    addCheckBox("debug", "Debug");
    plotDefs["debug0"] = {
        title:          getMessage("deBug0")+" -  ",
        checkbox:       "debug",
        plots: [
            {label:  '', dataFct:  function() {return SENSOR_DATA.debug[0]}}
        ]
    };

    plotDefs["debug1"] = {
        title:          getMessage("deBug1")+" -  ",
        checkbox:       "debug",
        plots: [
            {label:  '', dataFct:  function() {return SENSOR_DATA.debug[1]}}
        ]
    };

    plotDefs["debug2"] = {
        title:          getMessage("deBug2")+" -  ",
        checkbox:       "debug",
        plots: [
            {label:  '', dataFct:  function() {return SENSOR_DATA.debug[2]}}
        ]
    };

    plotDefs["debug3"] = {
        title:          getMessage("deBug3")+" -  ",
        checkbox:       "debug",
        plots: [
            {label:  '', dataFct:  function() {return SENSOR_DATA.debug[3]}}
        ]
    };   


    var plotsStats = getPlotCounts(checkboxDefs, plotDefs);
    var checkboxesQty = plotsStats.checkboxesQty;
    var axesPerPlots = plotsStats.axesPerPlots;
    var plotNames = plotsStats.plotNames;
    var plotsQty = plotsStats.plotsQty;
    var plotsCheckboxIndex = plotsStats.plotsCheckboxIndex;
   
    // Get the plots statistics (helps for the rest of the code)
    function getPlotCounts(checkboxDefs, plotDefs) {
        var axesPerPlots = [];
        var plotNames = [];
        var plotsCheckboxIndex = [];
        var checkboxesQty = checkboxDefs.checkboxesNames.length;
        var plotsIndex = 0;
        for (var plot in plotDefs) {
            if (!plotDefs.hasOwnProperty(plot)) {
                // the current property is not a direct property of plotDefs
                continue;
            }
            // Get basic properties
            plotNames[plotsIndex] = plot;
            axesPerPlots[plotsIndex] = plotDefs[plot].plots.length;
            
            // Find the index of the associated checkbox
            for (var j = 0; j < checkboxesQty; j++){
                var checkboxName = checkboxDefs.checkboxesNames[j];
                var plotCheckBox = plotDefs[plot].checkbox;
                if(checkboxName === plotCheckBox){
                    plotsCheckboxIndex[plotsIndex] = j;
                }
            }

            plotsIndex++;
        }
        var plotsQty = plotsIndex;
        
        return {
            checkboxesQty: checkboxesQty,
            plotsQty: plotsQty,
            axesPerPlots: axesPerPlots,
            plotNames: plotNames,
            plotsCheckboxIndex: plotsCheckboxIndex
        };
    }

    function initDataArray(length) {
        var data = new Array(length);
        for (var i = 0; i < length; i++) {
            data[i] = new Array();
            data[i].min = 0;
            data[i].max = 0;
        }
        return data;
    }

    function addSampleToData(data, sampleNumber, sensorData) {
        for (var i = 0; i < data.length; i++) {
            var dataPoint = sensorData[i];
            data[i].push([sampleNumber, dataPoint]);
            if (dataPoint < data[i].min) {
                data[i].min = dataPoint;
            }
            if (dataPoint > data[i].max) {
                data[i].max = dataPoint;
            }
        }
        while (data[0].length > 297) {
            for (i = 0; i < data.length; i++) {
                data[i].shift();
            }
        }
        return sampleNumber + 1;
    }

    var margin = {top: 20, right: 10, bottom: 10, left: 40};
    function updateGraphHelperSize(helpers) {
        helpers.width = helpers.targetElement.width() - margin.left - margin.right;
        helpers.height = helpers.targetElement.height() - margin.top - margin.bottom;

        helpers.widthScale.range([0, helpers.width]);
        helpers.heightScale.range([helpers.height, 0]);

        helpers.xGrid.tickSize(-helpers.height, 0, 0);
        helpers.yGrid.tickSize(-helpers.width, 0, 0);
    }

    function initGraphHelpers(selector, sampleNumber, heightDomain) {
        var helpers = {selector: selector, targetElement: $(selector), dynamicHeightDomain: true};

        helpers.widthScale = d3.scale.linear()
            .clamp(true)
            .domain([(sampleNumber - 299), sampleNumber]);

        helpers.heightScale = d3.scale.linear()
            .clamp(true)
            .domain(heightDomain || [-1, 1]);

        helpers.xGrid = d3.svg.axis();
        helpers.yGrid = d3.svg.axis();

        updateGraphHelperSize(helpers);

        helpers.xGrid
            .scale(helpers.widthScale)
            .orient("bottom")
            .ticks(5)
            .tickFormat("");

        helpers.yGrid
            .scale(helpers.heightScale)
            .orient("left")
            .ticks(5)
            .tickFormat("");

        helpers.xAxis = d3.svg.axis()
            .scale(helpers.widthScale)
            .ticks(5)
            .orient("bottom")
            .tickFormat(function (d) {return d;});

        helpers.yAxis = d3.svg.axis()
            .scale(helpers.heightScale)
            .ticks(5)
            .orient("left")
            .tickFormat(function (d) {return d;});

        helpers.line = d3.svg.line()
            .x(function (d) {return helpers.widthScale(d[0]);})
            .y(function (d) {return helpers.heightScale(d[1]);});

        return helpers;
    }

    function drawGraph(graphHelpers, data, sampleNumber, range) {
        var svg = d3.select(graphHelpers.selector);

        if (graphHelpers.dynamicHeightDomain) {
            var limits = [];
            $.each(data, function (idx, datum) {
                limits.push(datum.min);
                limits.push(datum.max);
            });
            limits = d3.extent(limits);
            var min = limits[0];
            //if (min < range[0]) min = range[0];
            var max = limits[1];
            //if (max > range[1]) max = range[1];
            
            //ensure minimum scale
            if(min > 0.01) min = 0.01;
            if(max < 0.01) max = 0.01;

            graphHelpers.heightScale.domain([min,max]);
        }
        graphHelpers.widthScale.domain([(sampleNumber - 299), sampleNumber]);

        svg.select(".x.grid").call(graphHelpers.xGrid);
        svg.select(".y.grid").call(graphHelpers.yGrid);
        svg.select(".x.axis").call(graphHelpers.xAxis);
        svg.select(".y.axis").call(graphHelpers.yAxis);

        var group = svg.select("g.data");
        var lines = group.selectAll("path").data(data, function (d, i) {return i;});
        var newLines = lines.enter().append("path").attr("class", "line");
        lines.attr('d', graphHelpers.line);
    }

    // add html code for generating plots
    function addPlotsHtml(checkboxName, plotDefs) {
        var result = [];

        // for each plot matching the current checkbox
        for (var plotIndex = 0; plotIndex < plotsQty; plotIndex++) {
            var plot = plotNames[plotIndex];
            if(checkboxName === plotDefs[plot].checkbox) {
                //using substring to set title and unit text
                var position = (plotDefs[plot].title).search("-");
                    var plotTitle = plotDefs[plot].title;
                    var plotUnit = "";
                    if (position > 0){
                        plotUnit = plotTitle.substr((position+1),plotTitle.length);
                        plotTitle = plotTitle.substr(0,(position-1));
                        
                    }

                //Make the html for this plot
                result += '<div class="plot_control">\n';
                result += '    <div class="title">' + plotTitle + '</div>\n';
                result += '    <dl>\n';
                result += '        <dd class="rate">\n';
                result += '        </dd>\n';
                var plots = plotDefs[plot].plots;
                var numberOfPlots = plots.length;
                if(numberOfPlots > 6) {
                    numberOfPlots = 6; // only 3 plots supported at this time
                }
                for (var i = 0; i < numberOfPlots; i++) {
                    result += '        <dt>'
                    var label = plots[i].label;
                    if(label != ''){
                        result += label + ':';
                    }
                    result += '</dt><dd class="';
                    switch (i) {
                        case 0:
                            result += 'x';
                            // $('.x dt').css();
                            break;
                        case 1:
                            result += 'y';
                            break;
                        case 2:
                            result += 'z';
                            break;
                        case 3:
                            result += 'a';
                            break;
                        case 4:
                            result += 'b';
                            break;
                        case 5:
                            result += 'c';
                            break;
                    }
                    result += " " + plot;
                    result += '">0</dd>';
                    result += '<dd class="unit">'+ plotUnit +'</dd>\n';
                }
                var labelWidth = 60;
                result += '    </dl>\n';
                result += '</div>\n';
                result += '<svg id="' + plot + '">\n'
                result += '    <g class="grid x" transform="translate(' + labelWidth + ', 120)"></g>\n';
                result += '    <g class="grid y" transform="translate(' + (labelWidth -10) + ', 10)"></g>\n';
                result += '    <g class="data" transform="translate(' + ((labelWidth+1) - 10) + ', 10)"></g>\n';
                result += '    <g class="axis x" transform="translate(' + (labelWidth - 10) + ', 120)"></g>\n';
                result += '    <g class="axis y" transform="translate(' + labelWidth + ', 10)"></g>\n';
                result += '</svg>\n';
                result += '<div class="clear-both"></div>\n';
            }
        }
        return result;
    }

    $('#plot').load("./tabs/sensors.html", function load_html() {
        //Reset axis button
        $(".resetIcon").click(function(){
          TABS.sensors.initialize();
          console.log("HEY");
        });  
      
        // fill in checkboxes definitions
        var checkboxesContent = [];
        for (var i = 0; i < checkboxesQty; i++) {
			checkboxesContent+='<label><input type="checkbox" name="' + checkboxDefs.checkboxesNames[i] + '"';
            if(i===0){
                checkboxesContent+=' class="first"';
            }
            checkboxesContent+= '/>' + getMessage(checkboxDefs.checkboxesLabels[i]) + '</label>' + '\n';
        }
        $('.tab-sensors .info .checkboxes').html(checkboxesContent);
        // create wrappers for each checkboxes
        var plotsWrappers = [];
        for (var i = 0; i < checkboxesQty; i++) {
            plotsWrappers += '<div class="wrapper ' + checkboxDefs.checkboxesNames[i] + '">\n';
            plotsWrappers += addPlotsHtml(checkboxDefs.checkboxesNames[i], plotDefs);
            plotsWrappers += '</div>';
        }
        $('.tab-sensors .plots').html(plotsWrappers);

        // Tooltip code
            $(".checkboxes img").tooltip({ show: { effect: "blind", duration: 100 } });
        // translate to user-selected language
        translateAll();
		
		var locallang=localStorage.getItem('lang');
        //Handle developper mode
        chrome.storage.local.get('DEBUG_MODE', function (result) {
             function hideDevMode(){
                $(".tab-sensors .info .checkboxes input[name='debug']").prop( "checked", false );
                $(".tab-sensors .info .checkboxes input[name='debug']").change();
                $(".tab-sensors .info .checkboxes input[name='debug']").parent().hide();

                $(".tab-sensors .info .checkboxes input[name='loadcells']").prop( "checked", false );
                $(".tab-sensors .info .checkboxes input[name='loadcells']").change();
                $(".tab-sensors .info .checkboxes input[name='loadcells']").parent().hide();
            }
            if (result.DEBUG_MODE != undefined){
                CONFIG.developper_mode = result.DEBUG_MODE;
                if(CONFIG.developper_mode){
                    activateDevelopperMode();
                }else{
                    hideDevMode();
                }
            }else{
                hideDevMode();
            }	
        });       
     
        $('.tab-sensors .info .checkboxes input').change(function () {
            var enable = $(this).prop('checked');
            var index = $(this).parent().index();

            if(!CONFIG.developper_mode && (checkboxDefs.checkboxesNames[index]==='debug' || checkboxDefs.checkboxesNames[index]==='loadcells')){
                enable = false;
            }

            //show/hide the plots under each checkbox
            var wrapper = '.wrapper.' + checkboxDefs.checkboxesNames[index];
            if (enable) {
                $(wrapper).show();
                googleAnalytics.sendEvent('Plot', 'Click', checkboxDefs.checkboxesNames[index]);
            } else {
                $(wrapper).hide();
            }

            var checkboxes = [];
            $('.tab-sensors .info .checkboxes input').each(function () {
                checkboxes.push($(this).prop('checked'));
            });
            PLOTSinfo.checkboxes = checkboxes;

            $('.tab-sensors .rate select:first').change();

            chrome.storage.local.set({'graphs_enabled': checkboxes});
        });

        function newFilledArray(length, val) {
            var array = [];
            for (var i = 0; i < length; i++) {
                array[i] = val;
            }
            return array;
        }

        // Setup variables
        var samples_i = newFilledArray(plotsQty,0);
        var plotData = [];
        var graphHelpers = [];
        for(var i = 0; i < plotsQty; i++) {  
            plotData[i] = initDataArray(axesPerPlots[i]);
            var plotName = plotNames[i];
            var selector = "#" + plotName;
            var sampleNumber = samples_i[i];
            graphHelpers[i] = initGraphHelpers(selector,sampleNumber,[-0.25,0.25]);
        }

        chrome.storage.local.get('graphs_enabled', function (result) {
            if (result.graphs_enabled) {
                var checkboxes = $('.tab-sensors .info .checkboxes input');
                for (var i = 0; i < result.graphs_enabled.length; i++) {
                    checkboxes.eq(i).not(':disabled').prop('checked', result.graphs_enabled[i]).change();
                }
            }
            else {
                //lt indicates witch checkboxes will be not checked by default
                $('.tab-sensors .info input:lt(2):not(:disabled)').prop('checked', true).change();
                var checkboxes = $('.tab-sensors .info .checkboxes input');
            }

        });

        //Save data to be used by the update_plots function
        PLOTSinfo.plotsQty = plotsQty;
        PLOTSinfo.plotNames = plotNames;
        PLOTSinfo.updateGraphHelperSize = updateGraphHelperSize;
        PLOTSinfo.graphHelpers = graphHelpers;
        PLOTSinfo.axesPerPlots = axesPerPlots;
        PLOTSinfo.plotDefs = plotDefs;
        PLOTSinfo.plotData = plotData;
        PLOTSinfo.samples_i = samples_i;
        PLOTSinfo.drawGraph = drawGraph;
        PLOTSinfo.addSampleToData = addSampleToData;
        PLOTSinfo.plotsCheckboxIndex = plotsCheckboxIndex;
        
        showBoardSpecific(false);
        selectPlotDefaultVal();
        if (callback) {
           if(typeof(callback)==="function") callback(); 
        }
    });

};

var refreshRate;
function refreshPlot(event) {
    refreshRate = (this.options[this.selectedIndex].value);
    changeMemory(CONFIG.plotsRateHz, refreshRate);
    clearInterval(USER_LIMITS.refreshRate);
    USER_LIMITS.refreshRate = setInterval(update_plots, refreshRate);  
    
}

function selectPlotDefaultVal(){
    $(function(){
        $('#plotRefreshRate').val(CONFIG.plotsRateHz.value);
    });        
}

// update plots function
function update_plots() {
    if(!SENSOR_DATA.newDataToPlot)
      return;
    SENSOR_DATA.newDataToPlot = false;
    for(var i = 0; i < PLOTSinfo.plotsQty; i++) {   
        var draw = PLOTSinfo.checkboxes[PLOTSinfo.plotsCheckboxIndex[i]];
        if(draw){
            var plotName = PLOTSinfo.plotNames[i];
            var newData = [];
            PLOTSinfo.updateGraphHelperSize(PLOTSinfo.graphHelpers[i]);
            for(var j = 0; j < PLOTSinfo.axesPerPlots[i]; j++) {
                var dataVal = PLOTSinfo.plotDefs[plotName].plots[j].dataFct();
                
                // avoid really small numbers
                if(Math.abs(dataVal) < 0.00001){
                    dataVal = 0;
                }
                
                // round to less digits accuracy
                dataVal = Number(dataVal.toPrecision(3));
                
                newData[j] = dataVal;

                // update text labels
                var displayValue = newData[j];
                var digits = 2;
                if(PLOTSinfo.plotDefs[plotName].hasOwnProperty('displayDigits')) {
                    digits = PLOTSinfo.plotDefs[plotName].displayDigits;
                }
                if(PLOTSinfo.plotDefs[plotName].hasOwnProperty('fixedDigits') &&
                PLOTSinfo.plotDefs[plotName].fixedDigits) {
                    displayValue = displayValue.toFixed(digits);
                }
                else {
                  //displayValue = displayValue.toPrecision(digits);
                }
                var classID = 'dd.';
                switch (j) {
                    case 0:
                        classID += 'x';
                        break;
                    case 1:
                        classID += 'y';
                        break;
                    case 2:
                        classID += 'z';
                        break;
                    case 3:
                        classID += 'a';
                        break;
                    case 4:
                        classID += 'b';
                        break;
                    case 5:
                        classID += 'c';
                        break;
                }
                classID += '.' + plotName;
                $(classID).text(displayValue); 
            }
            PLOTSinfo.samples_i[i] = PLOTSinfo.addSampleToData(PLOTSinfo.plotData[i], PLOTSinfo.samples_i[i], newData);
            var range = [-10000000.0, 10000000.0]
            if(PLOTSinfo.plotDefs[plotName].hasOwnProperty('range')) {
                    range = PLOTSinfo.plotDefs[plotName].range;
            }
            PLOTSinfo.drawGraph(PLOTSinfo.graphHelpers[i], PLOTSinfo.plotData[i], PLOTSinfo.samples_i[i], range);
        }
    }
    
    // fix display bug (vertical axis shows 0.20000000001)
    $("g.axis.y > g > text").each(function fixText(){
        var text = $( this ).text();    
        var val = Number(text);
        if(Math.abs(val)<0.0001) val = 0;
        var newText = Number(val.toPrecision(3)).toString();
        if(text !== newText){
           $( this ).text(newText);
        }
    });
}

TABS.sensors.cleanup = function (callback) {
    serial.emptyOutputBuffer();

    if (callback) callback();
};