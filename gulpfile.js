'use strict';
var mac = 'nwjs/Mac/';
var linux32 = 'nwjs/Linux-x32/';
var linux64 = 'nwjs/Linux/';
var windows = 'nwjs/Windows/';

var macApp = mac + 'nwjs.app/Contents/Resources/app.nw/';
var linux32App = linux32 + 'package.nw/';
var linux64App = linux64 + 'package.nw/';
var windowsApp = windows + 'package.nw/';

var gulp = require('gulp'),
    clean = require('gulp-clean'),
    cleanhtml = require('gulp-cleanhtml'),
    lintspaces = require('gulp-lintspaces'),
    sassRuby = require('gulp-ruby-sass'),
    concatCss = require('gulp-concat-css'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    eslint = require('gulp-eslint'),
    zip = require('gulp-zip');

// clean build directory
gulp.task('clean', function cleanTask() {
    return gulp.src(['build', macApp, linux32App, linux64App, windowsApp], {
        read: false
    }).pipe(clean());
});

// copy all non js/html content to build directory
// except "scripts" which should not be minified
// except "libraries" which are already minified
gulp.task('copy1', function copyTask() {
    return gulp.src(['src/js/libraries/**/*.js'])
        .pipe(gulp.dest('build/src/js/libraries'));
});
gulp.task('copy2', function copyTask() {
    return gulp.src(['src/**', '!src/**/*.js', '!src/**/*.html', '!src/tabs/autocontrol/rcbApi/out/**'])
        .pipe(gulp.dest('build/src'));
});
gulp.task('copy3', function copyTask() {
    return gulp.src(['src/scripts/**'])
        .pipe(gulp.dest('build/src/scripts'));
});

// copy and compress HTML files
gulp.task('html', function htmlTask() {
    return gulp.src(['src/**/*.html', '!src/tabs/autocontrol/rcbApi/out/**'])
        .pipe(cleanhtml())
        .pipe(gulp.dest('build/src'));
});

// process javascript, except the "scripts" and "libraries"
gulp.task('js', function jsAllTask() {
    return gulp.src(['src/**/*.js', '!src/scripts/*.js', '!src/scripts/database/*.js','!src/tabs/autocontrol/rcbApi/out/**','!src/js/libraries/**/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest('./build/src'));
});

// copy built source into each nwjs OS
gulp.task('build', ['html', 'js', 'copy1', 'copy2', 'copy3'], function buildTask() {
	return gulp.src(['build/src/**/*'])
        .pipe(gulp.dest(macApp))
        .pipe(gulp.dest(linux32App))
        .pipe(gulp.dest(linux64App))
        .pipe(gulp.dest(windowsApp));
});

// default task
gulp.task('default', ['clean'], function defaultTask() {
    return gulp.start('build');
});
